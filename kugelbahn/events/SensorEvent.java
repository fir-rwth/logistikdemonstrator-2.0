package kugelbahn.events;

import kugelbahn.KugelbahnEvent;
import kugelbahn.elektronik.Sensor;

public class SensorEvent extends KugelbahnEvent {
	public Sensor sensor;
	public boolean state;

	public SensorEvent(boolean isOn, Sensor sensor,long time ) {
		super( time );
		state = isOn;
		this.sensor = sensor;
	}
}
