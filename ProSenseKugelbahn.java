import com.tinkerforge.BrickletDistanceIR;
import com.tinkerforge.BrickletIO16;
import com.tinkerforge.IPConnection;
import com.tinkerforge.NotConnectedException;
import com.tinkerforge.TimeoutException;

import kugelbahn.*;
import kugelbahn.elements.*;
import kugelbahn.elektronik.*;
import kugelbahn.events.*;
import java.util.ArrayList;

public class ProSenseKugelbahn extends Kugelbahn {
	KugelbahnElektronik elektro;
	Section tM0M1, tM1W0, tW0M2, tW0M3, tM2W1, tW1M7Eil, tW1M7, tW1vorW2, tM3vorW2, tW2M4, tW2M5, tW2M5Eil, tM4M6,
			tM5M6, tM7End, tM6End, m0, m1, m2, m3, m4, m5, m6, m7, vorM1, vorM2, vorM3, vorM4, vorM6;
	Branch w0, w1, w2;
	Union vorW2, vorM5, vorM7, tM6;
	

	// diese Variablen geben die Anzahl einer Kugel wieder
	int anzKugelM0 = 0;
	int anzKugelM1 = 0;
	int anzKugelM2 = 0;
	int anzKugelM3 = 0;
	int anzKugelM4 = 0;
	int anzKugelM5 = 0;
	int anzKugelM6 = 0;
	int anzKugelM7 = 0;
	int kugelAnzA1 = 0;

	// dies Paramter geben die max. und min. erlaubte Anzahl an Kugeln vor jeder
	// Maschine
	int maxKugelAnzVorM1 = 6;
	int minKugelAnzVorM1 = 1;
	int maxKugelAnzVorM2 = 5;
	int minKugelAnzVorM2 = 1;
	int maxKugelAnzVorM3 = 4;
	int minKugelAnzVorM3 = 1;
	int maxKugelAnzVorM4 = 6;
	int minKugelAnzVorM4 = 1;
	int maxKugelAnzVorM5 = 5;
	int minKugelAnzVorM5 = 1;
	int maxKugelAnzVorM6 = 6;
	int minKugelAnzVorM6 = 1;
	int maxKugelAnzVorM7 = 4;
	int minKugelAnzVorM7 = 1;

	// zur Bestimmung, wie der Status der Sensoren ist (1 oder 0)
	boolean statusSM0V = false;
	boolean statusSM1V = false;
	boolean statusSM2V = false;
	boolean statusSM3V = false;
	boolean statusSM4V = false;
	boolean statusSM5V = false;
	boolean statusSM6V = false;
	boolean statusSM7V = false;
	boolean statusSM0Z = false;
	boolean statusSM1Z = false;
	boolean statusSM2Z = false;
	boolean statusSM3Z = false;
	boolean statusSM4Z = false;
	boolean statusSM5Z = false;
	boolean statusSM6Z = false;
	boolean statusSM7Z = false;

	// diese variablen werden für die erfolgung eines Eilauftrages benötigt
	boolean benutzerMax1 = false; // Max steht für MAXeKart: Maschine M7
	boolean benutzerMax2 = false;
	boolean benutzereGO1 = false; // eGO steht für e.GO: Maschine M5
	boolean benutzereGO2 = false; // EGo: Maschine M5

	// zur Istbestimmung der Kugelanzahl zu Beginn der Simulation
	boolean startM1 = false;
	boolean startM2 = false;
	boolean startM3 = false;
	boolean startM4 = false;
	boolean startM5 = false;
	boolean startM6 = false;
	boolean startM7 = false;
	boolean systemIstBereit = true;
	int eilAnzE1 = 0;
	int eilAnzW1 = 0;
	int eilAnzE2 = 0;
	int eilAnzW2 = 0;

	// Positionen im Bezug zur Kugelrollrichtung
	static final int LINKS = 0;
	static final int MITTE = 1;
	static final int RECHTS = 2;
	static final int AUF = 3;
	static final int ZU = 4;
	int istPos_E1 = LINKS;
	int sollPos_E1 = RECHTS;
	int istPos_E2 = LINKS;
	int sollPos_E2 = RECHTS;
	int istPos_W0 = RECHTS;
	int sollPos_W0 = LINKS;
	int istPos_W1 = LINKS;
	int sollPos_W1 = RECHTS;
	int istPos_W2 = LINKS;
	int sollPos_W2 = RECHTS;
	int istPos_A1 = AUF;
	int sollPos_A1 = ZU;
	int W2warten = 0;
	int startw1 = 0;

	boolean schranke_ist, schranke_soll, schranke_bewegt;
	long schranke_lastchange;
	final int schranke_zeit = 300;

	private Motor motor(String motorname) {
		return elektro.getMotor(motorname);
	}

	public ProSenseKugelbahn(KugelbahnElektronik elektro) throws Exception {
		this.elektro = elektro;

		// Motoren Bezeichnun, aufruf durch "start = new
		// MotorControlEvent(-32766, motor("M1"))" 32766 ist die max.
		// Geschwindigkeit
		ArrayList<Motor> motoren = new ArrayList<Motor>();
		motoren.add(new Motor("6qBSZF", "M0"));
		motoren.add(new Motor("68aTsH", "M1"));
		motoren.add(new Motor("6e69u2", "M2"));
		motoren.add(new Motor("68xHQD", "M3"));
		motoren.add(new Motor("6e8bAe", "M4"));
		motoren.add(new Motor("6e7vxX", "M5"));
		motoren.add(new Motor("6k55vx", "M6"));
		motoren.add(new Motor("6wVDcX", "M7"));
		motoren.add(new Motor("6kpC6p", "W0"));
		motoren.add(new Motor("68cyWM", "W2"));
		motoren.add(new Motor("6etmGH", "W1"));
		motoren.add(new Motor("68cdaP", "A1"));
		motoren.add(new Motor("6JMecp", "E1"));
		motoren.add(new Motor("68cQQY", "E2"));

		ArrayList<Sensor> sensorsT = new ArrayList<>();

		sensorsT.add(new ProSenseSensor(0, "SM0V", 'a'));
		sensorsT.add(new ProSenseSensor(1, "SM0H", 'a'));///
		sensorsT.add(new ProSenseSensor(4, "SM2H", 'a'));
		sensorsT.add(new ProSenseSensor(2, "SM2V", 'a'));
		sensorsT.add(new ProSenseSensor(7, "SM4V", 'a'));
		sensorsT.add(new ProSenseSensor(3, "SM7Z", 'a'));
		sensorsT.add(new ProSenseSensor(5, "RFIDb", 'a'));
		sensorsT.add(new ProSenseSensor(6, "SW2V", 'a'));
		sensorsT.add(new ProSenseSensor(3, "SM7V", 'b'));
		sensorsT.add(new ProSenseSensor(5, "SM5Z", 'b'));
		sensorsT.add(new ProSenseSensor(0, "SM5V", 'b'));
		sensorsT.add(new ProSenseSensor(2, "SM5H", 'b'));//
		sensorsT.add(new ProSenseSensor(4, "SM3V", 'b'));
		sensorsT.add(new ProSenseSensor(7, "SM4H", 'b'));
		sensorsT.add(new ProSenseSensor(1, "RFIDg", 'b'));
		sensorsT.add(new ProSenseSensor(6, "Eil-M5-Bahn", 'b'));

		ArrayList<Sensor> sensorsD = new ArrayList<>();

		sensorsD.add(new ProSenseSensor(0, "SM1Z", 'a'));
		sensorsD.add(new ProSenseSensor(1, "SM1V", 'a'));///
		sensorsD.add(new ProSenseSensor(2, "SM3Z", 'a'));
		sensorsD.add(new ProSenseSensor(3, "SM4Z", 'a'));
		sensorsD.add(new ProSenseSensor(4, "SM6V", 'a'));//
		sensorsD.add(new ProSenseSensor(5, "SM6Z", 'a'));
		sensorsD.add(new ProSenseSensor(6, "SM2Z", 'a'));
		sensorsD.add(new ProSenseSensor(7, "SM7H", 'a'));
		sensorsD.add(new ProSenseSensor(5, "SM6H", 'b'));
		sensorsD.add(new ProSenseSensor(6, "SM1H", 'b'));
		sensorsD.add(new ProSenseSensor(7, "SM3H", 'b'));

		ArrayList<IO16> ios = new ArrayList<IO16>();

		IO16 brickletT = new IO16("qK3", sensorsT);
		IO16 brickletD = new IO16("qKa", sensorsD);

		ios.add(brickletT);
		ios.add(brickletD);

		ArrayList<String> masters = new ArrayList<String>();

		masters.add("68cxuV");
		masters.add("6xgN6P");
		masters.add("5VF9oN");

		TFStack stack = new TFStack("localhost", 4223, masters, motoren, ios);

		this.elektro.addStacks(stack);
		
		MotorControlEvent start = new MotorControlEvent(5000, motor("W1"));
		createEvent(start);
		MotorControlEvent stop = new MotorControlEvent(0, motor("W1"), 1000);
		createEvent(stop);
		/*
		 * this.addElement(tM0M1 = new Section("tM0M1")); this.addElement(vorM1
		 * = new Section("vorM1", tM0M1)); this.addElement(m1 = new
		 * Section("m1", vorM1)); this.addElement(w0 = new Branch("w0", 0, m1));
		 * this.addElement(tW0M2 = new Section("tW0M2", w0));
		 * this.addElement(vorM2 = new Section("vorM2", tW0M2));
		 * this.addElement(m2 = new Section("m2", vorM2)); this.addElement(w1 =
		 * new Branch("w1",0, m2)); this.addElement(tW1M7Eil = new
		 * Section("tW1M7Eil", w1)); this.addElement(vorM7 = new Union
		 * ("vorM7",tW1M7Eil, tW1M7)); this.addElement(tW1M7 = new
		 * Section("tW1M7", w1)); this.addElement(m7 = new Section("m7",
		 * vorM7)); this.addElement(tW1vorW2 = new Section("tW1vorW2",w1));
		 * this.addElement(tW0M3 = new Section("tW0M3", w0));
		 * this.addElement(vorM3 = new Section("vorM3", tW0M3));
		 * this.addElement(m3 = new Section("m3", vorM3));
		 * this.addElement(tM3vorW2 = new Section("tM3vorW2", vorW2));
		 * this.addElement(vorW2 = new Union("vorW2", tM3vorW2, tW1vorW2));
		 * this.addElement(w2 = new Branch("w2",0, vorW2));
		 * this.addElement(tW2M5Eil = new Section("tW2M5Eil", vorM5));
		 * this.addElement(vorM5 = new Union("vorM7",tW2M5Eil, tW2M5));
		 * this.addElement(tW2M5 = new Section("tW2M5", vorM5));
		 * this.addElement(m5 = new Section("m5", tM6)); this.addElement(tW2M4 =
		 * new Section("tW2M4", vorM4)); this.addElement(vorM4 = new
		 * Section("vorM4", m4)); this.addElement(m4 = new Section("m4", tM6));
		 * this.addElement(tM6 = new Union ("tM6", m4,m5));
		 * this.addElement(vorM6 = new Section("vorM6", m6)); this.addElement(m6
		 * = new Section("m6", tM6End));
		 */
	}

	// ------------------------------------------------------------------------------------------------------------------------
	void startAllMotors(KugelbahnEvent e) {
		MotorControlEvent start;
		if (e instanceof ProgramStartEvent) {

			start = new MotorControlEvent(-26000, motor("M0"));
			createEvent(start);
			start = new MotorControlEvent(-26000, motor("M1"));
			createEvent(start);
			start = new MotorControlEvent(-22000, motor("M2"));
			createEvent(start);
			start = new MotorControlEvent(22000, motor("M3"));
			createEvent(start);
			start = new MotorControlEvent(22000, motor("M4"));
			createEvent(start);
			start = new MotorControlEvent(22000, motor("M5"));
			createEvent(start);
			start = new MotorControlEvent(22000, motor("M6"));
			createEvent(start);
			start = new MotorControlEvent(-22000, motor("M7"));
			createEvent(start);
		}
	}

	void stopAllMotors(KugelbahnEvent e) {
		MotorControlEvent stop;
		if (e instanceof ProgramStopEvent) {
			stop = new MotorControlEvent(0, motor("M0"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M1"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M2"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M3"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M4"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M5"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M6"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M7"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("W0"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("W1"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("W2"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("A1"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("E1"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("E2"));
			createEvent(stop);

		}

	}

	/**
	 * Methode, die alle Motoren steuern sollte
	 * 
	 * @param e
	 *            Events (Ereignisse), die zur Steuerung notwendig sind
	 * @param motorname
	 *            Der Name der Machine, die gesteuert werden soll
	 * @param maxKugelAnzVor
	 *            die maximal erlaubte Anzahl an Kugeln vor einer Maschine
	 * @param minKugelAnzVor
	 *            die minimal erlaubte Anzahl an Kugeln vor einer Maschine
	 * @param maxKugelAnzNach
	 *            die maximal erlaubte Gesamtanzahl an Kugeln vor der nächsten
	 *            Maschine
	 * 
	 *            void logicMotoren_1(KugelbahnEvent e, String motorname) { int
	 *            maxKugelAnzVor=3; int minKugelAnzVor=1; int maxKugelAnzNach=3;
	 * 
	 *            MotorControlEvent start; String[] nachfolger= new String [3];
	 * 
	 * 
	 *            //die maximale und minimale Anzahl der Kugeln individuel
	 *            bestimmen
	 * 
	 *            switch (motorname) { case "M1": nachfolger[0] = "M2";
	 *            nachfolger [1] = "M3"; break; case "M2": nachfolger[0]= "M4";
	 *            nachfolger[1]= "M5"; nachfolger[2]= "M7"; break; case "M3":
	 *            nachfolger[0]= "M4"; nachfolger[1]= "M5"; break;
	 * 
	 *            default: break; }
	 * 
	 * 
	 * 
	 *            if (e instanceof ProgramStartEvent) { start = new
	 *            MotorControlEvent(-20000, motor(motorname));
	 *            createEvent(start); if (e instanceof SensorEvent) {
	 *            SensorEvent se = (SensorEvent) e; if
	 *            (se.sensor.sensorname.equals("S"+motorname+"V") ||
	 *            se.sensor.sensorname.equals("S"+motorname+"H")) { //
	 *            anzKugelM2 und anzKugelM1 müssen noch aus den Sektionen
	 *            abgefragt werden if (anzKugelM2 >= maxKugelAnzNach ||
	 *            anzKugelM1 <= minKugelAnzVor) { start = new
	 *            MotorControlEvent(-10000, motor(motorname));
	 *            createEvent(start); }else{ if (anzKugelM1 >= maxKugelAnzVor) {
	 *            start = new MotorControlEvent(-30000, motor(motorname));
	 *            createEvent(start); }else{ start = new
	 *            MotorControlEvent(-20000, motor(motorname));
	 *            createEvent(start); } } } } } }
	 * 
	 *            void transfering1(KugelbahnEvent e) { if (e instanceof
	 *            SensorEvent) { SensorEvent se = (SensorEvent) e; if (se.state)
	 *            { switch (se.sensor.sensorname) { case "SM0H": KugelbahnKugel
	 *            kugel=new KugelbahnKugel(id); KugelCreateEvent kCreat = new
	 *            KugelCreateEvent(kugel, tM0M1); createEvent(kCreat);
	 *            KugelTransferEvent kTrans = new KugelTransferEvent(tM0M1,0);
	 *            createEvent(kTrans); id++; break; case "SM1V": kTrans = new
	 *            KugelTransferEvent(tM0M1, 0); createEvent(kTrans); break; case
	 *            "SM1Z": kTrans = new KugelTransferEvent(vorM1, 0);
	 *            createEvent(kTrans); break; case "SM1H": kTrans = new
	 *            KugelTransferEvent(m1,0); createEvent(kTrans); kTrans = new
	 *            KugelTransferEvent(w0,300); createEvent(kTrans); break;
	 * 
	 *            case "SM2V": kTrans = new KugelTransferEvent(tW0M2, 0);
	 *            createEvent(kTrans); break; case "SM2Z": kTrans = new
	 *            KugelTransferEvent(vorM2, 0); createEvent(kTrans); break; case
	 *            "SM2H": kTrans = new KugelTransferEvent(m2,400);
	 *            createEvent(kTrans); kTrans = new KugelTransferEvent(w1,500);
	 *            createEvent(kTrans); break; case "SM3V": kTrans = new
	 *            KugelTransferEvent(tW0M3, 0); createEvent(kTrans); break; case
	 *            "SM3Z": kTrans = new KugelTransferEvent(vorM3, 0);
	 *            createEvent(kTrans); break; case "SM3H": kTrans = new
	 *            KugelTransferEvent(m3,0); createEvent(kTrans); break; case
	 *            "SM4V": kTrans = new KugelTransferEvent(tW2M4, 0);
	 *            createEvent(kTrans); break; case "SM4Z": kTrans = new
	 *            KugelTransferEvent(vorM4, 0); createEvent(kTrans); break; case
	 *            "SM4H": kTrans = new KugelTransferEvent(m4,0);
	 *            createEvent(kTrans); break; case "SM5V": kTrans = new
	 *            KugelTransferEvent(tW2M5, 0); createEvent(kTrans); break; case
	 *            "SM5Z": kTrans = new KugelTransferEvent(vorM5, 0);
	 *            createEvent(kTrans); break; case "SM5H": kTrans = new
	 *            KugelTransferEvent(m5,0); createEvent(kTrans); break;
	 * 
	 * 
	 * 
	 *            default: break; } }
	 * 
	 * 
	 *            }
	 * 
	 *            }
	 */
	void transfering(KugelbahnEvent e) {

		if (e instanceof IntegerEvent) {
			int delay = 1000;
			IntegerEvent ie = (IntegerEvent) e;
			switch (ie.value) {
			case 0:
				if (statusSM0V) {
					sollPos_A1 = ZU;
					IntegerEvent ie1 = new IntegerEvent(0, delay);
					createEvent(ie1);
					istPos_A1();
				}
				break;

			case 1:
				if (statusSM1V) {
					startM1 = false;
					anzKugelM1 = 8;
					IntegerEvent ie1 = new IntegerEvent(1, delay);
					createEvent(ie1);
					// MotorControlEvent start = new MotorControlEvent(-25000,
					// motor("M1"));
					// createEvent(start);
					// System.out.println("(1)M1-->"+anzKugelM1);

				}
				break;

			case 2:
				if (statusSM2V) {
					startM2 = false;
					anzKugelM2 = 6;
					IntegerEvent ie1 = new IntegerEvent(2, delay);
					createEvent(ie1);
					// MotorControlEvent start = new MotorControlEvent(-30000,
					// motor("M2"));
					// createEvent(start);
					// System.out.println("(1)M2--->"+anzKugelM2);
				}
				break;

			case 3:
				if (statusSM3V) {
					startM3 = false;
					anzKugelM3 = 4;
					IntegerEvent ie1 = new IntegerEvent(3, delay);
					createEvent(ie1);
					// MotorControlEvent start = new MotorControlEvent(28000,
					// motor("M3"));
					// createEvent(start);
					// System.out.println("(1)M3--->"+anzKugelM3);
				}
				break;
			case 4:
				if (statusSM4V) {
					startM4 = false;
					anzKugelM4 = 6;
					IntegerEvent ie1 = new IntegerEvent(4, delay);
					createEvent(ie1);
					// MotorControlEvent start = new MotorControlEvent(28000,
					// motor("M4"));
					// createEvent(start);
					// System.out.println("(1)M4--->"+anzKugelM4);
				}
			case 5:
				if (statusSM5V) {
					startM5 = false;
					anzKugelM5 = 5;
					IntegerEvent ie1 = new IntegerEvent(5, delay);
					createEvent(ie1);
					// MotorControlEvent start = new MotorControlEvent(28000,
					// motor("M5"));
					// createEvent(start);
					// System.out.println("(1)M5--->"+anzKugelM5);
				}
				break;
			case 6:
				if (statusSM6V) {
					startM6 = false;
					anzKugelM6 = 6;
					IntegerEvent ie1 = new IntegerEvent(6, delay);
					createEvent(ie1);
					// MotorControlEvent start = new MotorControlEvent(26000,
					// motor("M6"));
					// createEvent(start);
					// System.out.println("(1)M6--->"+anzKugelM6);
				}
				break;
			case 7:
				if (statusSM7V) {
					startM7 = false;
					anzKugelM7 = 4;
					IntegerEvent ie1 = new IntegerEvent(7, delay);
					createEvent(ie1);
					// MotorControlEvent start = new MotorControlEvent(-30000,
					// motor("M7"));
					// createEvent(start);
					// System.out.println("(1)M7--->"+anzKugelM7);
				}
				break;

			default:
				break;
			}
		}

		if (e instanceof SensorEvent) {
			SensorEvent se = (SensorEvent) e;
			// if (se.sensor.sensorname.equals("SM0V") && se.state) {
			// anzKugelM0++;
			// System.out.println("Anzahl bei M0: " + anzKugelM0);
			// }
			// if (se.sensor.sensorname.equals("SM0H") && !se.state) {
			// anzKugelM0--;
			// System.out.println("Anzahl bei M0: " + anzKugelM0);
			//
			// }

			//
			// if (se.sensor.sensorname.equals("SM1V") && se.state) {
			// anzKugelM1++;
			//// System.out.println("Anzahl bei M1: " + anzKugelM1);
			// }
			// --------------------------------------------------Anzahl der
			// Aufträge vor und in der jeweiligen Maschine
			// bestimmen----------------------------------------
			// ----------M1----------------------------
			if (se.sensor.sensorname.equals("SM0V")) {
				IntegerEvent ie1 = new IntegerEvent(0, 500);
				createEvent(ie1);
				statusSM0V = se.state;
			}

			if (se.sensor.sensorname.equals("SM1V")) {
				IntegerEvent ie1 = new IntegerEvent(1, 500);
				createEvent(ie1);
				statusSM1V = se.state;
				if (se.state) {
					if (anzKugelM1 < maxKugelAnzVorM1) {
						anzKugelM1++;
					}
					System.out.println("Anzahl bei M1: " + anzKugelM1);
					
					if(startw1 == 0){
						MotorControlEvent start = new MotorControlEvent(-3000, motor("W1"));
						createEvent(start);
						MotorControlEvent stop = new MotorControlEvent(0, motor("W1"), 1000);
						createEvent(stop);
						istPos_W1 = RECHTS;
						start = new MotorControlEvent(20000, motor("E1"));
						createEvent(start);
						stop = new MotorControlEvent(0, motor("E1"), 1000);
						createEvent(stop);
						istPos_E1 = RECHTS;
						start = new MotorControlEvent(2500, motor("W2"));
						createEvent(start);
						stop = new MotorControlEvent(0, motor("W2"), 1000);
						createEvent(stop);
						istPos_W2=RECHTS;
						
						
						startw1++;
						
					}
				}
			}

			if (se.sensor.sensorname.equals("SM1Z")) {
				if (anzKugelM1 > 0) {
					anzKugelM1--;
				}
				System.out.println("Anzahl bei M1: " + anzKugelM1);
			}

			// ------M2----------------------------------------------
			if (se.sensor.sensorname.equals("SM2V")) {
				IntegerEvent ie2 = new IntegerEvent(2, 500);
				createEvent(ie2);
				statusSM2V = se.state;
				if (se.state) {
					if (anzKugelM2 < maxKugelAnzVorM2){
						anzKugelM2++;
					}
					System.out.println("Anzahl bei M2: " + anzKugelM2);
				}
			}
			if (se.sensor.sensorname.equals("SM2Z")) {
				if (se.state) {
					if (anzKugelM2 > 0) {
						anzKugelM2--;
					}
					System.out.println("Anzahl bei M2: " + anzKugelM2);
				}
			}

			// ----------------------------------M3--------------------------------------
			if (se.sensor.sensorname.equals("SM3V")) {
				IntegerEvent ie3 = new IntegerEvent(3, 500);
				createEvent(ie3);
				statusSM3V = se.state;
				if (se.state) {
					if (anzKugelM3 < maxKugelAnzVorM3)
						anzKugelM3++;
					System.out.println("Anzahl bei M3: " + anzKugelM3);
				}
			}
			if (se.sensor.sensorname.equals("SM3Z")) {
				if (se.state) {
					if (anzKugelM3 > 0) {
						anzKugelM3--;
					}
					System.out.println("Anzahl bei M3: " + anzKugelM3);
				}
			}
			// --------------------------------------M4----------------------------
			if (se.sensor.sensorname.equals("SM4V")) {
				IntegerEvent ie4 = new IntegerEvent(4, 500);
				createEvent(ie4);
				statusSM4V = se.state;
				if (se.state) {
					if (anzKugelM4 < maxKugelAnzVorM4)
						anzKugelM4++;
					System.out.println("Anzahl bei M4: " + anzKugelM4);
				}
			}
			if (se.sensor.sensorname.equals("SM4Z")) {
				if (se.state) {
					if (anzKugelM4 > 0) {
						anzKugelM4--;
					}
					System.out.println("Anzahl bei M4: " + anzKugelM4);
				}
			}
			// ----------------------------M5---------------------------------
			if (se.sensor.sensorname.equals("SM5V")) {
				IntegerEvent ie5 = new IntegerEvent(5, 500);
				createEvent(ie5);
				statusSM5V = se.state;
				if (se.state) {
					if (anzKugelM5 < maxKugelAnzVorM5)
						anzKugelM5++;
					System.out.println("Anzahl bei M5: " + anzKugelM5);
				}
			}
			if (se.sensor.sensorname.equals("SM5Z")) {
				if (se.state) {
					if (anzKugelM5 > 0) {
						anzKugelM5--;
					}
					System.out.println("Anzahl bei M5: " + anzKugelM5);
				}
			}
			// ---------------------------M6-----------------------------------
			if (se.sensor.sensorname.equals("SM6V")) {
				IntegerEvent ie6 = new IntegerEvent(6, 500);
				createEvent(ie6);
				statusSM6V = se.state;
				if (se.state) {
					if (anzKugelM6 < maxKugelAnzVorM6)
						anzKugelM6++;
					System.out.println("Anzahl bei M6: " + anzKugelM6);
				}
			}
			if (se.sensor.sensorname.equals("SM6Z")) {
				if (se.state) {
					if (anzKugelM6 > 0) {
						anzKugelM6--;
					}
					System.out.println("Anzahl bei M6: " + anzKugelM6);
				}
			}
			// ------------------------M7----------------------------------
			if (se.sensor.sensorname.equals("SM7V")) {
				IntegerEvent ie7 = new IntegerEvent(7, 500);
				createEvent(ie7);
				statusSM7V = se.state;
				if (se.state) {
					if (anzKugelM7 < maxKugelAnzVorM7)
						anzKugelM7++;
					System.out.println("Anzahl bei M7: " + anzKugelM7);
				}
			}
			if (se.sensor.sensorname.equals("SM7Z")) {
				if (se.state) {
					if (anzKugelM7 > 0) {
						anzKugelM7--;
					}
					System.out.println("Anzahl bei M7: " + anzKugelM7);
				}
			}
		}

	}

	/*
	 * void intEventAus(KugelbahnEvent e){
	 * 
	 * if (e instanceof IntegerEvent){ IntegerEvent ie= (IntegerEvent)e; switch
	 * (ie.value) { case 2: anzKugelM2=6; break; case 3: anzKugelM2=0; break;
	 * case 4: anzKugelM3=4; break; case 5: anzKugelM3=0; break;
	 * 
	 * default: break; } } }
	 */

	// -----------------------------------------------------Prozess----------------------------------------------------------------------------------
	// --------------------------------------System muss betriebsbereit
	// sein-----------------------------------------------------
	void systemStatus(KugelbahnEvent e) {
		if (e instanceof SensorEvent) {
			if (!systemIstBereit && !startM1 && !startM2 && !startM3 && !startM4 && !startM5 && !startM6 && !startM7) {
				systemIstBereit = true;
				System.out.println("Der Demonstrator ist betriebsbereit");
			}
		}
	}

	// -----------------------------------------------------M0----------------------------------------------------------------------------------
	/**
	 * Methode, die alle Motoren steuern sollte
	 * 
	 * @param e
	 *            Events (Ereignisse), die zur Steuerung notwendig sind
	 * @param motorname
	 *            Der Name der Machine, die gesteuert werden soll
	 * @param maxKugelAnzVor
	 *            die maximal erlaubte Anzahl an Kugeln vor einer Maschine
	 * @param minKugelAnzVor
	 *            die minimal erlaubte Anzahl an Kugeln vor einer Maschine
	 * @param maxKugelAnzNach
	 *            die maximal erlaubte Gesamtanzahl an Kugeln vor der nächsten
	 *            Maschine
	 */
	void logicMotoren_0(KugelbahnEvent e) {
		// int maxKugelAnzVor = 6;
		// int minKugelAnzVor = 1;
		// int maxKugelAnzNach = 3;

		MotorControlEvent start;

		if (e instanceof ProgramStartEvent) {
			start = new MotorControlEvent(-26000, motor("M0"));
			createEvent(start);
		} else {
			if (e instanceof SensorEvent) {
				SensorEvent se = (SensorEvent) e;
				if (systemIstBereit || statusSM1V) {
					// anzKugelM1 muss noch aus den Sektionen abgefragt werden
					if (anzKugelM1 >= maxKugelAnzVorM1) {
						start = new MotorControlEvent(0, motor("M0"));
						createEvent(start);
					} else {
						start = new MotorControlEvent(-26000, motor("M0"));
						createEvent(start);

					}
				} /*
					 * if(se.sensor.sensorname.equals("SM0V")){ KugelbahnKugel
					 * kk = new KugelbahnKugel(id); id++; KugelCreateEvent kce =
					 * new KugelCreateEvent(kk, t0); createEvent(kce);
					 * 
					 * }
					 */
			}
		}
	}

	// -----------------------------------------------------M1----------------------------------------------------------------------------------
	void logicMotoren_1(KugelbahnEvent e) {
		// int maxKugelAnzVor = 3;
		// int minKugelAnzVor = 1;
		// int maxKugelAnzNach = 3;

		MotorControlEvent start;

		String[] nachfolger = new String[3];

		if (e instanceof ProgramStartEvent) {
			start = new MotorControlEvent(-24000, motor("M1"));
			createEvent(start);
		} else {
			if (e instanceof SensorEvent) {
				SensorEvent se = (SensorEvent) e;
				if (!startM1) {
					// anzKugelM2 und anzKugelM1 müssen noch aus den Sektionen
					// abgefragt werden
					if ((anzKugelM2 >= maxKugelAnzVorM2 && anzKugelM3 >= maxKugelAnzVorM3
							|| anzKugelM1 <= minKugelAnzVorM1) && !startM2 && !startM3) {
						start = new MotorControlEvent(-24000, motor("M1"), 0);
						createEvent(start);
						// System.out.println("Geschwindigkeit von M1: langsam
						// ");
					} else {
						if (anzKugelM1 >= maxKugelAnzVorM1) {
							start = new MotorControlEvent(-26000, motor("M1"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M1:
							// schnell ");
						} else {
							start = new MotorControlEvent(-26000, motor("M1"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M1:
							// mittel");
						}
					}
				}
			}
		}
	}

	// -----------------------------------------------------M2----------------------------------------------------------------------------------
	void logicMotoren_2(KugelbahnEvent e) {
		// int maxKugelAnzVor = 5;
		// int minKugelAnzVor = 1;
		// int maxKugelAnzNach = 3;

		MotorControlEvent start;

		String[] nachfolger = new String[3];

		if (e instanceof ProgramStartEvent) {
			start = new MotorControlEvent(-24000, motor("M2"));
			createEvent(start);

		} else {
			if (e instanceof SensorEvent) {
				SensorEvent se = (SensorEvent) e;
				if (!startM2) {
					// anzKugelM2 und anzKugelM1 müssen noch aus den Sektionen
					// abgefragt werden
					if ((anzKugelM7 >= maxKugelAnzVorM7
							&& (anzKugelM4 >= maxKugelAnzVorM4 && anzKugelM5 >= maxKugelAnzVorM5)
							|| anzKugelM2 <= minKugelAnzVorM2) && !startM4 && !startM5 && !startM7) {
						start = new MotorControlEvent(-24000, motor("M2"));
						createEvent(start);
						// System.out.println("Geschwindigkeit von M2: langsam
						// ");
					} else {
						if (anzKugelM2 >= maxKugelAnzVorM2) {
							start = new MotorControlEvent(-26000, motor("M2"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M2:
							// schnell ");
						} else {
							start = new MotorControlEvent(-24000, motor("M2"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M2:
							// mittel");
						}
					}
				}
			}
		}
	}

	// -----------------------------------------------------M3----------------------------------------------------------------------------------
	void logicMotoren_3(KugelbahnEvent e) {
		// int maxKugelAnzVor = 3;
		// int minKugelAnzVor = 1;
		// int maxKugelAnzNach = 3;

		MotorControlEvent start;

		String[] nachfolger = new String[3];

		if (e instanceof ProgramStartEvent) {
			start = new MotorControlEvent(2400, motor("M3"));
			createEvent(start);
		} else {
			if (e instanceof SensorEvent) {
				SensorEvent se = (SensorEvent) e;
				if (!startM3) {
					// anzKugelM4 & anzKugelM5 und anzKugelM3 müssen noch aus
					// den Sektionen abgefragt werden
					if ((anzKugelM4 >= maxKugelAnzVorM4 && anzKugelM5 >= maxKugelAnzVorM5
							|| anzKugelM3 <= minKugelAnzVorM3) && !startM4 && !startM5) {
						start = new MotorControlEvent(24000, motor("M3"));
						createEvent(start);
						// System.out.println("Geschwindigkeit von M3: langsam
						// ");
					} else {
						if (anzKugelM3 >= maxKugelAnzVorM3) {
							start = new MotorControlEvent(26000, motor("M3"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M3:
							// schnell ");
						} else {
							start = new MotorControlEvent(24000, motor("M3"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M3:
							// mittel");
						}
					}
				}
			}
		}
	}

	// -----------------------------------------------------M4----------------------------------------------------------------------------------
	void logicMotoren_4(KugelbahnEvent e) {
		// int maxKugelAnzVor = 3;
		// int minKugelAnzVor = 1;
		// int maxKugelAnzNach = 3;

		MotorControlEvent start;

		String[] nachfolger = new String[3];

		if (e instanceof ProgramStartEvent) {
			start = new MotorControlEvent(20000, motor("M4"));
			createEvent(start);
		} else {
			if (e instanceof SensorEvent) {
				SensorEvent se = (SensorEvent) e;
				if (!startM4) {
					// anzKugelM6 und anzKugelM4 müssen noch aus den Sektionen
					// abgefragt werden
					if ((anzKugelM6 >= maxKugelAnzVorM6 || anzKugelM4 <= minKugelAnzVorM4) && !startM6) {
						start = new MotorControlEvent(24000, motor("M4"));
						createEvent(start);
						// System.out.println("Geschwindigkeit von M4: langsam
						// ");
					} else {
						if (anzKugelM4 >= maxKugelAnzVorM4) {
							start = new MotorControlEvent(26000, motor("M4"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M4:
							// schnell ");
						} else {
							start = new MotorControlEvent(24000, motor("M4"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M4:
							// mittel");
						}
					}
				}
			}
		}
	}

	// -----------------------------------------------------M5----------------------------------------------------------------------------------
	void logicMotoren_5(KugelbahnEvent e) {
		// int maxKugelAnzVor = 3;
		// int minKugelAnzVor = 1;
		// int maxKugelAnzNach = 3;

		MotorControlEvent start;

		String[] nachfolger = new String[3];

		if (e instanceof ProgramStartEvent) {
			start = new MotorControlEvent(24000, motor("M5"));
			createEvent(start);
		} else {
			if (e instanceof SensorEvent) {
				SensorEvent se = (SensorEvent) e;
				if (!startM5) {
					// anzKugelM6 und anzKugelM5 müssen noch aus den Sektionen
					// abgefragt werden
					if ((anzKugelM6 >= maxKugelAnzVorM6 || anzKugelM5 <= minKugelAnzVorM5) && !startM6) {
						start = new MotorControlEvent(24000, motor("M5"));
						createEvent(start);
						// System.out.println("Geschwindigkeit von M5: langsam
						// ");
					} else {
						if (anzKugelM5 >= maxKugelAnzVorM5) {
							start = new MotorControlEvent(26000, motor("M5"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M5:
							// schnell ");
						} else {
							start = new MotorControlEvent(24000, motor("M5"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M5:
							// mittel");
						}
					}
				}
			}
		}
	}

	// -----------------------------------------------------M6----------------------------------------------------------------------------------
	void logicMotoren_6(KugelbahnEvent e) {
		// int maxKugelAnzVor = 3;
		// int minKugelAnzVor = 1;
		// int maxKugelAnzNach = 3;

		MotorControlEvent start;

		String[] nachfolger = new String[3];

		if (e instanceof ProgramStartEvent) {
			start = new MotorControlEvent(24000, motor("M6"));
			createEvent(start);
		} else {
			if (e instanceof SensorEvent) {
				SensorEvent se = (SensorEvent) e;
				if (!startM6 && systemIstBereit) {
					// anzKugelM6 müssen noch aus den Sektionen abgefragt werden
					if (anzKugelM6 <= minKugelAnzVorM6) {
						start = new MotorControlEvent(24000, motor("M6"));
						createEvent(start);
					} else {
						if (anzKugelM6 >= maxKugelAnzVorM6) {
							start = new MotorControlEvent(26000, motor("M6"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M1:
							// schnell ");
						} else {
							start = new MotorControlEvent(24000, motor("M6"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M6:
							// mittel");
						}
					}
				}
			}
		}
	}

	// -----------------------------------------------------M7----------------------------------------------------------------------------------
	void logicMotoren_7(KugelbahnEvent e) {
		// int maxKugelAnzVor = 3;
		// int minKugelAnzVor = 1;
		// int maxKugelAnzNach = 3;

		MotorControlEvent start;
		String[] nachfolger = new String[3];

		if (e instanceof ProgramStartEvent) {
			start = new MotorControlEvent(-24000, motor("M7"));
			createEvent(start);
		} else {
			if (e instanceof SensorEvent) {
				SensorEvent se = (SensorEvent) e;
				if (!startM7 && systemIstBereit) {
					// anzKugelM7 müssen noch aus den Sektionen abgefragt werden
					if (anzKugelM7 <= minKugelAnzVorM7) {
						start = new MotorControlEvent(-24000, motor("M7"));
						createEvent(start);
						// System.out.println("Geschwindigkeit von M7: langsam
						// ");
					} else {
						if (anzKugelM7 >= maxKugelAnzVorM7) {
							start = new MotorControlEvent(-26000, motor("M7"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M7:
							// schnell ");
						} else {
							start = new MotorControlEvent(-24000, motor("M7"));
							createEvent(start);
							// System.out.println("Geschwindigkeit von M7:
							// mittel");
						}
					}
				}
			}
		}

	}
	// ----------------------------------------------------------Weichen-initialisierung------------------------------------------------------------------------------------

	void setWeichen(KugelbahnEvent e) {
		if (e instanceof ProgramStartEvent) {

			istPos_E1();
			// System.out.println("Init E1 ist rechts");

			istPos_E2();
			// System.out.println("Init E1 ist rechts");

			istPos_W0();
			// System.out.println("Init W0 ist links");

			istPos_W1();
			// System.out.println("Init W1 ist rechts");

			istPos_W2();
			// System.out.println("Init W2 ist rechts");

			istPos_A1();
			// System.out.println("Init A1 ist zu");
		}

	}

	// -----------------------------------------------------------Weiche
	// 0-------------------------------------------------------------------------------------------
	/**
	 * Stellt die Weiche W0, nach der SollPosition um
	 *
	 * TODO:WeichenParameter richtig bestimmen
	 *
	 */
	void istPos_W0() {
		MotorControlEvent start, stop;

		if (sollPos_W0 != istPos_W0) {
			if (istPos_W0 == LINKS) {
				start = new MotorControlEvent(-32000, motor("W0"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("W0"), 500);
				createEvent(stop);
				istPos_W0 = sollPos_W0;
				System.out.println("W0 ist Rechts");
			} else {
				start = new MotorControlEvent(32000, motor("W0"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("W0"), 500);
				createEvent(stop);
				istPos_W0 = sollPos_W0;
				System.out.println("W0 ist Links");
			}
		}
	}

	void sollPos_W0(KugelbahnEvent e) {
		if (e instanceof SensorEvent) {
			SensorEvent se = (SensorEvent) e;
			if (/*
				 * se.sensor.sensorname.equals("SM2V") ||
				 * se.sensor.sensorname.equals("SM3V")||
				 */ se.sensor.sensorname.equals("SM1H")) {

				if ((anzKugelM2 > anzKugelM3 || anzKugelM2 == maxKugelAnzVorM2)) {
					sollPos_W0 = RECHTS;
					System.out.println("(1)W0 soll nach rechts");
				} else if (anzKugelM2 < anzKugelM3 || anzKugelM3 == maxKugelAnzVorM3) {
					sollPos_W0 = LINKS;
					System.out.println("(2)W0 soll nach links");
				} else {
					sollPos_W0 = LINKS;
					System.out.println("(3)W0 soll nach links");
				}

				/*
				 * if ((anzKugelM2 == anzKugelM3)&&!statusSM3V&&!statusSM2V) {
				 * if ((anzKugelM2 % 2) == 0) { sollPos_W0 = RECHTS;
				 * System.out.println("(3)W0 soll nach rechts"); } else {
				 * sollPos_W0 = LINKS; System.out.println(
				 * "(4)W0 soll nach links"); } }
				 */
				System.out.println("istPost_W0 wir aufgerufen");
				istPos_W0();
			}
		}
	}
	// -------------------------------------------------Schranke--------------------------------------------------------------------------------

	void istPos_A1() {
		MotorControlEvent start, stop;
		if (sollPos_A1 != istPos_A1) {
			if (istPos_A1 == ZU) {
				start = new MotorControlEvent(20000, motor("A1"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("A1"), 1000);
				createEvent(stop);
				istPos_A1 = sollPos_A1;
				// System.out.println("A1 ist auf");
			} else {
				if (istPos_A1 == AUF) {
					start = new MotorControlEvent(-20000, motor("A1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("A1"), 1000);
					createEvent(stop);
					istPos_A1 = sollPos_A1;
					IntegerEvent ie = new IntegerEvent(10, 30000);
					createEvent(ie);
					// System.out.println("A1 ist zu");
				}
			}
		}

	}

	void sollPos_A1(KugelbahnEvent e) {
		// Umwandlung in ein SensorEvent-Objekt

		if (e instanceof IntegerEvent) {
			IntegerEvent ie = (IntegerEvent) e;

			if (ie.value == 0) {
				sollPos_A1 = ZU;
				System.out.println("A1 soll zu");
				istPos_A1();
			}
			if (ie.value == 10) {
				sollPos_A1 = AUF;
				System.out.println("A1 soll auf");
				istPos_A1();
			}
		}

		if (e instanceof SensorEvent) {
			SensorEvent se = (SensorEvent) e;
			if (se.sensor.sensorname.equals("SM0V")) {
				IntegerEvent ie = new IntegerEvent(0, 1000);
				createEvent(ie);
				statusSM0V = se.state;
			}
		}
		if (e instanceof ProgramStartEvent) {
			IntegerEvent ie = new IntegerEvent(0, 1000);
			createEvent(ie);
		}

	}

	// -------------------------------------------------WeicheW1--------------------------------------------------------------------------------

	/**
	 * Stellt die Weiche W1, nach der SollPosition um
	 *
	 * TODO:WeichenParameter richtig bestimmen
	 *
	 */
	void istPos_W1() {
		MotorControlEvent start, stop;
		if (sollPos_W1 != istPos_W1) {
			if (istPos_W1 == LINKS) {
				if (sollPos_W1 == RECHTS) {
					start = new MotorControlEvent(-3000, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Rechts (1)");
				} else {
					start = new MotorControlEvent(-1700, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Mitte(2)");

				}
			} else if (istPos_W1 == RECHTS) {
				if (sollPos_W1 == LINKS) {
					start = new MotorControlEvent(3000, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Links(3)");
				} else {
					start = new MotorControlEvent(1500, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Mitte(4)");

				}
			} else if (istPos_W1 == MITTE) {
				if (sollPos_W1 == LINKS) {
					start = new MotorControlEvent(2000, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Links(5)");
				} else {
					start = new MotorControlEvent(-2000, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Rechts(6)");

				}
			}

		}

	}

	void sollPos_W1(KugelbahnEvent e) {
		if (e instanceof SensorEvent) {
			if (!benutzerMax1 && eilAnzW1 == 0) {
				SensorEvent se = (SensorEvent) e;
				if (se.sensor.sensorname.equals("SM2H") && se.state && !benutzereGO1) {
					if (anzKugelM7 > anzKugelM4 || anzKugelM7 > anzKugelM5 || anzKugelM7 == maxKugelAnzVorM7) {
						sollPos_W1 = RECHTS;
						System.out.println("(1)W1 soll nach rechts");
					} else if (anzKugelM7 < anzKugelM4 || anzKugelM7 < anzKugelM5
							|| anzKugelM4 == maxKugelAnzVorM4 && anzKugelM5 == maxKugelAnzVorM5) {
						sollPos_W1 = MITTE;
						System.out.println("(2)W1 soll nach mitte");
					} else if (anzKugelM4 == anzKugelM7) {
						sollPos_W1 = RECHTS;
						System.out.println("(3)W1 soll nach rechts");
					}

					// if (anzKugelM7 == ((anzKugelM4 + anzKugelM5) / 2)) {
					/*
					 * if ((anzKugelM7 % 2) == 0) { sollPos_W1 = RECHTS;
					 * System.out.println("(3)W1 soll nach rechts"); } else {
					 * sollPos_W1 = MITTE; System.out.println(
					 * "(4)W1 soll nach mitte"); } // }
					 */

					System.out.println("istPos_W1 wird aufgerufen");
					istPos_W1();
				}
			}
		}
	}

	// -------------------------------------------Weiche
	// E1-----------------------------------------------------

	// Der Beginn bzw. das Ende des Eilauftrags wird erfasst und die
	// Sollposition der Weiche E1 bestimmt.
	void eilAufMax(KugelbahnEvent e) {
		if (e instanceof SensorEvent) {
			SensorEvent se = (SensorEvent) e;
			if (se.sensor.sensorname.equals("RFIDg") && se.state) {
				benutzerMax1 = true;
				benutzerMax2 = true;
				eilAnzE1++;
				eilAnzW1 = eilAnzE1;
				// System.out.println("EilAnzE1 ++: "+eilAnzE1);
				// System.out.println("EilAnzW1 ++: "+eilAnzW1);
			} else if (se.sensor.sensorname.equals("SM2H") && !se.state && benutzerMax1) {
				MotorControlEvent start = new MotorControlEvent(18000, motor("M2"));
				sollPos_W1 = LINKS;
				istPos_W1();
				// System.out.println("W1 soll Links");
				sollPos_E1 = LINKS;
				// System.out.println("E1 soll Links");
				istPos_E1();
				eilAnzW1--;
				if (eilAnzW1 == 0) {
					benutzerMax1 = false;
				}
				// System.out.println("EilAnzW1 --: "+eilAnzW1);
			} else if (se.sensor.sensorname.equals("SM7Z") && benutzerMax2 && se.state && !benutzerMax1
					&& istPos_E1 == LINKS) {
				if (istPos_E1 == LINKS && eilAnzE1 > 0) {
					eilAnzE1--;
					// System.out.println("EilAnzE1 --: "+eilAnzE1);
				}
				if (eilAnzE1 == 0) {
					sollPos_E1 = RECHTS;
					benutzerMax2 = false;
					// System.out.println("E1 soll Rechts");
					istPos_E1();

				}
			}

		}
	}

	// Die Weiche E1 wird nach den Angaben der vorher bestimmten Sollposition
	// umgestellt
	void istPos_E1() {
		MotorControlEvent start, stop;
		if (sollPos_E1 != istPos_E1) {
			if (istPos_E1 == LINKS) {
				start = new MotorControlEvent(20000, motor("E1"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("E1"), 1000);
				createEvent(stop);
				istPos_E1 = sollPos_E1;
				// System.out.println("E1 ist Rechts");
			} else if (istPos_E1 == RECHTS) {
				start = new MotorControlEvent(-20000, motor("E1"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("E1"), 1000);
				createEvent(stop);
				istPos_E1 = sollPos_E1;
				// System.out.println("E1 ist Links");
			}
		}
	}

	// -------------------------------------------------WeicheW2--------------------------------------------------------------------------------

	/**
	 * Stellt die Weiche W2, nach der SollPosition um
	 *
	 * TODO:WeichenParameter richtig bestimmen
	 *
	 */
	void istPos_W2() {
		MotorControlEvent start, stop;
		if (sollPos_W2 != istPos_W2) {
			if (istPos_W2 == LINKS) {
				if (sollPos_W2 == RECHTS) {
					start = new MotorControlEvent(2500, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Rechts (1)");
				} else {
					start = new MotorControlEvent(1400, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Mitte(2)");

				}
			} else if (istPos_W2 == RECHTS) {
				if (sollPos_W2 == LINKS) {
					start = new MotorControlEvent(-2500, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Links(3)");
				} else {
					start = new MotorControlEvent(-1400, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Mitte(4)");

				}
			} else if (istPos_W2 == MITTE) {
				if (sollPos_W2 == LINKS) {
					start = new MotorControlEvent(-2000, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Links(5)");
				} else {
					start = new MotorControlEvent(2000, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Rechts(6)");

				}
			}

		}

	}

	void sollPos_W2(KugelbahnEvent e) {
		if (e instanceof SensorEvent) {
			SensorEvent se = (SensorEvent) e;
			if (se.sensor.sensorname.equals("SW2V") && !benutzereGO1) {
				if (W2warten == 1) {
					System.out.println("Warten");
					W2warten = 0;
				} else if (anzKugelM5 > anzKugelM4 || anzKugelM5 == maxKugelAnzVorM5) {
					sollPos_W2 = RECHTS;
					System.out.println("(1)W2 soll nach rechts");
				} else if (anzKugelM5 < anzKugelM4 || anzKugelM4 == maxKugelAnzVorM4) {
					sollPos_W2 = MITTE;
					System.out.println("(2)W2 soll nach mitte");
				} else if (anzKugelM5 == anzKugelM4) {
					sollPos_W2 = RECHTS;
					System.out.println("(3)W2 soll nach rechts");
				}

				/*
				 * if (anzKugelM5 == anzKugelM4) { if ((anzKugelM5 % 2) == 0) {
				 * sollPos_W2 = RECHTS; // System.out.println(
				 * "(3)W2 soll nach rechts"); } else { sollPos_W2 = MITTE;
				 * System.out.println("(4)W2 soll nach mitte"); } }
				 */

				System.out.println("istpos_W2 wird aufgerufen");
				istPos_W2();
			}
		}
	}

	// -------------------------------------------Weiche
	// E2-----------------------------------------------------

	// Der Beginn bzw. das Ende des Eilauftrags wird erfasst und die
	// Sollposition der Weiche E1 bestimmt.
	void eilAufeGO(KugelbahnEvent e) {
		if (e instanceof SensorEvent) {
			SensorEvent se = (SensorEvent) e;
			if (se.sensor.sensorname.equals("RFIDb") && se.state) {
				W2warten = 1;
				benutzereGO1 = true;
				benutzereGO2 = true;
				eilAnzE2++;
				eilAnzW2 = eilAnzE2;
				System.out.println("EilAnzE2 ++: " + eilAnzE2);
				System.out.println("EilAnzW2 ++: " + eilAnzW2);
			} else if (((se.sensor.sensorname.equals("SM2H") && (istPos_W1 == RECHTS))
					|| se.sensor.sensorname.equals("SM3H")) && benutzereGO1) {

				sollPos_W2 = LINKS;
				System.out.println("W2 soll Links");
				istPos_W2();
				sollPos_E2 = LINKS;
				System.out.println("E2 soll Links");
				istPos_E2();
				eilAnzW2--;
				if (eilAnzW2 == 0) {
					benutzereGO1 = false;
				}
				System.out.println("EilAnzW2 --: " + eilAnzW2);
			} else if (se.sensor.sensorname.equals("SM5Z") && benutzereGO2 && se.state && !benutzereGO1
					&& istPos_E2 == LINKS) {
				if (istPos_E2 == LINKS && eilAnzE2 > 0) {
					eilAnzE2--;
					System.out.println("EilAnzE2 --: " + eilAnzE2);
				}
				if (eilAnzE2 == 0) {
					sollPos_E2 = RECHTS;
					benutzereGO2 = false;
					System.out.println("E2 soll Rechts");
					istPos_E2();

				}
			}

		}
	}

	// Die Weiche E2 wird nach den Angaben der vorher bestimmten Sollposition
	// umgestellt
	void istPos_E2() {
		MotorControlEvent start, stop;
		if (sollPos_E2 != istPos_E2) {
			if (istPos_E2 == LINKS) {
				start = new MotorControlEvent(-32000, motor("E2"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("E2"), 1000);
				createEvent(stop);
				istPos_E2 = sollPos_E2;
				System.out.println("E2 ist Rechts");
			} else {
				if (istPos_E2 == RECHTS) {
					start = new MotorControlEvent(32000, motor("E2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("E2"), 1000);
					createEvent(stop);
					istPos_E2 = sollPos_E2;
					System.out.println("E2 ist Links");
				}
			}
		}
	}

	/** Event Handler */
	protected boolean handleEvent(KugelbahnEvent e) {
		// System.out.println("Handling Event of type " +
		// e.getClass().getName());
		/*
		 * if( e instanceof SensorEvent &&
		 * ((SensorEvent)e).sensor.sensorname.equals( "SM1H" ) ) { try {
		 * MotorControlEvent start = new MotorControlEvent( 30000, motor("W0"));
		 * createEvent(start); MotorControlEvent stop = new MotorControlEvent(0,
		 * motor("W0"),300); createEvent(stop);
		 * 
		 * } catch( Exception ex ) { }; }
		 */

		if (e instanceof SensorEvent) {
			SensorEvent eprime = (SensorEvent) e;
			ProSenseSensor sensorprime = (ProSenseSensor) (eprime.sensor);
			sensorprime.simulatedstate = eprime.state;
		}
		startAllMotors(e);
		setWeichen(e);
		eilAufMax(e);
		eilAufeGO(e);
		transfering(e);
		sollPos_A1(e);
		sollPos_W0(e);
		sollPos_W1(e);
		sollPos_W2(e);
		logicMotoren_0(e);
		logicMotoren_1(e);
		logicMotoren_2(e);
		logicMotoren_3(e);
		logicMotoren_4(e);
		logicMotoren_5(e);
		logicMotoren_6(e);
		logicMotoren_7(e);
		systemStatus(e);
		// if( e instanceof ProgramStopEvent )
		// System.out.println( "STOP!!!" );
		stopAllMotors(e);
		return true;
	}
}
