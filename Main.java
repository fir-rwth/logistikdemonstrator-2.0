import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import kugelbahn.*;
import kugelbahn.elektronik.*;
import kugelbahn.events.ProgramStartEvent;
import kugelbahn.ShutdownHandler;

public class Main {
	static String server = "http://188.166.62.95";
	static int fetchIntervall = 4000;
	static int threadIntervall = 100;

	public static void main(String[] args) throws Exception {

		KugelbahnElektronik elektro = new KugelbahnElektronik();

		if (args != null && args.length == 1) {
			int szenario = Integer.parseInt(args[0]);
			if (szenario == 1) {
				ProSenseKugelbahnVerfolgung demo1 = new ProSenseKugelbahnVerfolgung(elektro, 1);
				System.out.println("Verfolgungs-Szenario 1 erstellt");
				ShutdownHandler shutdown = new ShutdownHandler(demo1);
				Thread elektroThread = new Thread(elektro);
				elektroThread.start();
				demo1.createEvent(new ProgramStartEvent());
				Runtime.getRuntime().addShutdownHook(new Thread(shutdown));
				

				while (true) {
					elektro.passEvents(demo1);
					demo1.process();

					//Thread.sleep(threadIntervall);
				}
			} else if (szenario == 2) {
				ProSenseKugelbahnVerfolgung demo2 = new ProSenseKugelbahnVerfolgung(elektro, 2);
				System.out.println("Verfolgungs-Szenario 2 erstellt");
				ShutdownHandler shutdown = new ShutdownHandler(demo2);
				Thread elektroThread = new Thread(elektro);
				elektroThread.start();
				demo2.createEvent(new ProgramStartEvent());
				Runtime.getRuntime().addShutdownHook(new Thread(shutdown));

				while (true) {
					elektro.passEvents(demo2);
					demo2.process();

					//Thread.sleep(threadIntervall);
				}
			}
		}

		ProSenseKugelbahn demo = new ProSenseKugelbahn(elektro);
		ShutdownHandler shutdown = new ShutdownHandler(demo);

		/* Create KUGELBAHN object */

		/*
		 * Start processing in two threads. Elektro is the high priority thread,
		 * main is the low priority thread.
		 */
		Thread elektroThread = new Thread(elektro);
		elektroThread.start();

		int lastParsed = -1;
		int iteration = 0;

		demo.createEvent(new ProgramStartEvent());
		Runtime.getRuntime().addShutdownHook(new Thread(shutdown));

		while (true) {
			JSONArray newOrders = null;

			/*
			 * if( fetchIntervall>iteration*( threadIntervall++ ) ) { iteration
			 * = 0;
			 * 
			 * try { String orderList = Communicator.getText(
			 * server+"/orders?sortBy=confirmedAt&sort=ASC" ); JSONObject data =
			 * new JSONObject( orderList );
			 * 
			 * newOrders = data.getJSONArray( "result" ); } catch( Exception e )
			 * { System.out.println(
			 * "WARNING: Could not fetch new orders. Connection to server failed."
			 * ); }
			 * 
			 * if( newOrders!=null ) { boolean parsing = false;
			 * 
			 * for( int i = 0; i<newOrders.length(); i++ ) { JSONObject
			 * candidate = newOrders.getJSONObject( i ); int id =
			 * candidate.getInt( "id" );
			 * 
			 * if( lastParsed==(-1) || parsing ) { if( candidate.getString(
			 * "status" ).equals( "confirmed" ) ) { System.out.println(
			 * "New Order: "+id+" "+candidate.getString( "product" ) );
			 * demo.pushOrder( new Order( id,candidate.getString( "product" ) )
			 * ); lastParsed = id; }
			 * 
			 * parsing = true; } else if( id==lastParsed ) parsing = true; } } }
			 */

			elektro.passEvents(demo);
			demo.process();

			Thread.sleep(threadIntervall);
		}
	}

}
