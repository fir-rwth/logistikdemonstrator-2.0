import kugelbahn.elektronik.Sensor;

class ProSenseSensor extends Sensor {
	public boolean simulatedstate;
	
	public ProSenseSensor( int bitNumber, String sensorname, char port ) {
		super( bitNumber,sensorname,port );
	}
}
