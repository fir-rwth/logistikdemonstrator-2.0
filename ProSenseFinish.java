import java.util.ArrayList;
import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONObject;

import kugelbahn.*;

public class ProSenseFinish implements KugelbahnElementListener {
	ArrayList<Integer> stageIds = new ArrayList<>( );
	ArrayList<String> stageNames = new ArrayList<>( );
	
	ProSenseKugelbahn kugelbahn;
	
	LinkedList<FeedbackEvent> transfers = new LinkedList<>();
	
	public ProSenseFinish( ) throws Exception {
		String stageList = Communicator.getText( Main.server+"/stages" );
		JSONArray stages = new JSONObject( stageList ).getJSONArray( "result" );
		
		for( int i = 0; i<stages.length(); i++ ) {
			JSONObject stage = stages.getJSONObject( i );
			stageIds.add( stage.getInt( "id" ) );
			stageNames.add( stage.getString( "name" ) );
		}
	}
	
	public void setKugelbahn( ProSenseKugelbahn kg ) {
		this.kugelbahn = kg;
	}
	
	public void received(Order o, KugelbahnElement el ) {
		KugelbahnElement real;
		
		if( !el.ident.equals( "t5") && !el.ident.equals( "t6" ) ) {
			if( el.ident.equals( "t4" ) ) {
				real = el.getNext().getNext();
			} else
				real = el;
			
			this.transfers.add( new FeedbackEvent( o,real,true ) );
		}
	}
	
	public void lost( Order o, KugelbahnElement el ) {
		
		if( !el.ident.equals( "t4" ) ) {
			this.transfers.add( new FeedbackEvent( o,el,false ) );
		}
	}
	
	public void send( ) {
		while( transfers.size()>0 ) {
			FeedbackEvent ev = transfers.peek();
			int orderid = ev.order.id;
			try {
				int stageid = getStageId( ev.section.ident );
				String type = ev.type?"in":"out";
				try {
					//System.out.println( "Order: "+orderid+" Stage: "+ev.section.ident+" Type: "+type );
					Communicator.getText( Main.server+"/orderstages/create?OrderId="+orderid+"&StageId="+stageid+"&eventType="+type );
					transfers.removeFirst( );
				} catch (Exception e) {
					break;
				}
			} catch (Exception e1) {
				System.out.println( "Could not find StageId for stage "+ev.section.ident );
				transfers.removeFirst( );
			}
		}
	}
	
	private int getStageId( String stagename ) throws Exception {
		for( int i = 0; i<stageIds.size(); i++ ) {
			if( stageNames.get( i ).equals( stagename ) )
				return stageIds.get( i );
		}
		
		throw new Exception( );
	}

}
