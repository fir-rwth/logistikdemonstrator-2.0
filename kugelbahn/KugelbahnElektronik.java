package kugelbahn;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayDeque;
import java.util.ArrayList;

import com.tinkerforge.*;
import kugelbahn.elektronik.*;
import kugelbahn.events.SensorEvent;

public class KugelbahnElektronik implements Runnable {
	static int connectRelaxTime = 3000;
	static int threadInterval = 10;

	ArrayList<TFStack> stacks = new ArrayList<>();
	ArrayDeque<SensorEvent> eventStack = new ArrayDeque<>();

	static public IPConnection connect( TFStack stack ) throws NotConnectedException,TimeoutException,AlreadyConnectedException,InterruptedException,UnknownHostException,IOException {
		IPConnection ipConPre = new IPConnection( );
		ipConPre.connect( stack.host,stack.port );
		for (String brickname : stack.bricknames) {
			BrickMaster master = new BrickMaster( brickname,ipConPre );
			master.reset();
		}
		Thread.sleep( connectRelaxTime );
		ipConPre.disconnect( );
		Thread.sleep( connectRelaxTime );

		IPConnection ipCon = new IPConnection();
		ipCon.connect( stack.host,stack.port);
		
		return ipCon;
	}
	
	public void run( ) {
		while( true ) {	
			for (int i = 0; i < stacks.size(); i++) {
				TFStack stack = stacks.get(i);
	
				for (int j = 0; j < stack.io16bricklets.size(); j++) {
					IO16 bricklet = stack.io16bricklets.get(j);
				
					try {
						char[ ] portNames= { 'a','b' };
						
						for( int k = 0; k<portNames.length; k++ ) {
							int currentStates = bricklet.tfobject.getPort( portNames[ k ] );
							int portChanges =( currentStates^bricklet.currentvalue[ k ] )& bricklet.sensorMasks[ k ];
					
							for( int l = 0;( portChanges>>l )!=0; l++ ) {
								if( ( ( portChanges>>l )&1 )!=0 ) {								
									boolean currentState =( ( currentStates>>l )& 1 )!=0;
									
									Sensor sensor = null;
									
									for( int m = 0; m<bricklet.sensors.size(); m++ ) {
										sensor = bricklet.sensors.get( m );
										if( sensor.port==portNames[ k ]&& sensor.bitNumber==l )
											break;
									}
									
									if( sensor.isActive() ) {
										SensorEvent e = new SensorEvent( currentState,sensor,System.currentTimeMillis() );
										
										if( !currentState )
											sensor.deactivate( );

										synchronized ( this.eventStack ) {
											this.eventStack.addLast(e);
										}
										
										if( currentState )
											bricklet.currentvalue[ k ]|= 1<<l;
										else
											bricklet.currentvalue[ k ]^= 1<<l;
									}
										
								}
									
							}
						}
					} catch (TimeoutException | NotConnectedException e) {
					}
				}
			}
			
			try {
				Thread.sleep( threadInterval );
			} catch (InterruptedException e) {
			}
		}
	}

	public KugelbahnElektronik( TFStack... stacks_array )throws TimeoutException,NotConnectedException,UnknownHostException,AlreadyConnectedException,IOException,InterruptedException {
		addStacks( stacks_array );
	}

	public void addStacks( TFStack... stacks_array )throws TimeoutException,NotConnectedException,UnknownHostException,AlreadyConnectedException,IOException,InterruptedException {
		for( int i = 0; i < stacks_array.length; i++ )
			stacks.add( stacks_array[i] );

		for( int i = 0; i < stacks.size( ); i++ ) {
			TFStack stack = stacks.get( i );

			IPConnection ipCon = connect( stack );
			ipCon.setAutoReconnect( true );

			for( int j = 0; j < stack.io16bricklets.size( ); j++ ) {
				IO16 bricklet = stack.io16bricklets.get( j );

				bricklet.tfobject = new BrickletIO16(bricklet.brickletname,ipCon);

				bricklet.tfobject.setPortConfiguration( 'a',(short)bricklet.sensorMasks[ 0 ],'i',false );
				bricklet.tfobject.setPortConfiguration( 'a',(short)( ~( bricklet.sensorMasks[ 0 ] ) ),'o',false );
				bricklet.tfobject.setPortConfiguration( 'b',(short)bricklet.sensorMasks[ 1 ],'i',false);
				bricklet.tfobject.setPortConfiguration( 'b',(short)( ~( bricklet.sensorMasks[ 1 ] ) ),'o',false );
			}

			for (int j = 0; j < stacks.get(i).motoren.size(); j++) {
				Motor motor = stacks.get(i).motoren.get(j);
				if(stacks.get(i).motoren.get(j).motorname.startsWith("M") || stacks.get(i).motoren.get(j).motorname == "W0"){
				motor.setTFObject( new BrickDC( stacks.get( i ).motoren.get( j ).brickname,ipCon ) );
				}else{
				motor.setTFObjectWeichen( new BrickDC( stacks.get( i ).motoren.get( j ).brickname,ipCon ) );
				}
//				System.out.println(stacks.get(i).motoren.get(j).motorname);
//				System.out.println(stacks.get(i).motoren.get(j).motorname.startsWith("M"));
				}
		}
	}
	
	public Motor getMotor( String motorname ) {
		for (int i = 0; i < this.stacks.size(); i++) {
			TFStack stack = this.stacks.get(i);
			for (int j = 0; j < stack.motoren.size(); j++) {
				Motor motor = stack.motoren.get(j);
				if (motor.motorname == motorname)
					return motor;
			}
		}
		return null;
	}

	public void passEvents( KugelbahnEventHandler h ) {
		synchronized( eventStack ) {
			while( eventStack.size( )!=0 ) {
				SensorEvent e = eventStack.pop();
				h.createEvent( e );
			}
		}
	}
	
	public int clearEvents( ) {
		int eventCount;
		synchronized( eventStack ) {
			eventCount = eventStack.size();
			eventStack.clear();
		}
		
		return eventCount;
	}
}
