package kugelbahn.events;

import kugelbahn.KugelbahnEvent;
import kugelbahn.elements.Branch;

public class BranchChangeEvent extends KugelbahnEvent {
	public Branch target;
	public int direction;
	
	public BranchChangeEvent( Branch target,int direction ) {
		this.target = target;
		this.direction = direction;
	}
}
