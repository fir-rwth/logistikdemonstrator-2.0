package kugelbahn.elektronik;

import com.tinkerforge.BrickDC;
import com.tinkerforge.NotConnectedException;
import com.tinkerforge.TimeoutException;

public class Motor {
	public String brickname;
	public String motorname;
	private short velocity;
	private short accel;
	private BrickDC tfobject;

	public Motor( String brickname,String motorname ) {
		this.brickname = brickname;
		this.motorname = motorname;
		this.velocity = 0;
	}
	
	public Motor( String brickname,String motorname,int velocity ) {
		this.brickname = brickname;
		this.motorname = motorname;
		this.velocity = (short)velocity;
	}

	public void setTFObject( BrickDC tfo ) throws TimeoutException,NotConnectedException {
		tfobject = tfo;
		tfobject.setAcceleration( 0 );
		tfobject.setDriveMode( (short)1 );
		this.setVelocity( this.velocity );
	}
	
	public void setTFObjectWeichen( BrickDC tfo ) throws TimeoutException,NotConnectedException {
		tfobject = tfo;
		tfobject.setAcceleration( 0 );
		tfobject.setDriveMode( (short)0 );
		tfobject.setPWMFrequency(1);
		this.setVelocity( this.velocity );
	}
	

	public void setVelocity( int velocity ) throws TimeoutException,NotConnectedException {
		this.velocity = (short)velocity;
		if( this.velocity!=0 ) {
			tfobject.setVelocity( this.velocity );
			tfobject.enable( );
		} else
			tfobject.disable( );
	}
}
