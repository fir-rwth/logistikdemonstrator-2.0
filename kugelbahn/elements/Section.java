package kugelbahn.elements;

import kugelbahn.KugelbahnElement;

public class Section extends KugelbahnElement {

	public Section( String dbgi,KugelbahnElement... ke ) throws Exception {
		super( dbgi,ke );
		
		if (ke.length > 1) {
			throw new Exception();
		}
	}

	public KugelbahnElement getNext() {
		return this.outputs.get( 0 );
	}

}
