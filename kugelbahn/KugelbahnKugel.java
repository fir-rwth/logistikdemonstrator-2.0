package kugelbahn;

public class KugelbahnKugel {
	public int id;

	public KugelbahnKugel( int id ) {
		this.id = id;
	}
	
	public String toString( ) {
		return Integer.toString( id );
	}
}
