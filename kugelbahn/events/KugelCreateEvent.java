package kugelbahn.events;

import kugelbahn.KugelbahnEvent;
import kugelbahn.KugelbahnKugel;
import kugelbahn.KugelbahnElement;

public class KugelCreateEvent extends KugelbahnEvent {
	public KugelbahnKugel kugel;
	public KugelbahnElement element;
	public boolean atFront;
	
	public KugelCreateEvent( KugelbahnKugel kugel,KugelbahnElement where ) {
		this.kugel = kugel;
		this.element = element;
		atFront = true;
	}
}
