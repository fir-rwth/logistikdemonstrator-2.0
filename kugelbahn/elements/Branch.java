package kugelbahn.elements;

import kugelbahn.KugelbahnElement;
import kugelbahn.KugelbahnKugel;

public class Branch extends KugelbahnElement {
	public int direction;
	
	public Branch(String dbgi,int dir,KugelbahnElement ke) {
		super(dbgi,ke);
		this.direction = dir;
	}

	public KugelbahnElement getNext() {
		return this.outputs.get( direction );
	}
	
	public void addKugel( KugelbahnKugel o ) {
		this.getNext().addKugel( o );
	}
	
	public String toString( ) {
		return "==< ("+this.getNext( ).ident+")";
	}
}
