package kugelbahn.events;

import kugelbahn.KugelbahnEvent;
import kugelbahn.KugelbahnElement;

public class KugelTransferEvent extends KugelbahnEvent {
	public KugelbahnElement source;
	
	public KugelTransferEvent( KugelbahnElement source,long delay ) {
		super( System.currentTimeMillis( )+delay );
		this.source = source;	
	}
}
