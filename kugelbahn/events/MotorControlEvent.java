package kugelbahn.events;

import kugelbahn.KugelbahnEvent;
import kugelbahn.elektronik.Motor;

public class MotorControlEvent extends KugelbahnEvent {
	public int speed;
	public Motor motor;

	public MotorControlEvent( int speed,Motor motor ) {
		this.speed = speed;
		this.motor = motor;
	}
	
	public MotorControlEvent( int speed,Motor motor,long delay ) {
		super( System.currentTimeMillis( )+delay );
		this.speed = speed;
		this.motor = motor;
	}
}
