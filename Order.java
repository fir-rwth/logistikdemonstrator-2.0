import kugelbahn.KugelbahnKugel;

public class Order extends KugelbahnKugel {
	public String productname;

	public Order( int id,String name ) {
		super( id );
		this.productname = name;
	}
}
