package kugelbahn;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

import kugelbahn.events.MotorControlEvent;
import kugelbahn.events.KugelTransferEvent;
import kugelbahn.events.KugelChangeEvent;
import kugelbahn.events.KugelCreateEvent;
import kugelbahn.events.BranchChangeEvent;

public abstract class Kugelbahn implements KugelbahnEventHandler {
	protected ArrayList< KugelbahnElement > elements = new ArrayList< KugelbahnElement >( );
	protected LinkedList< KugelbahnEvent > eventqueue = new LinkedList< KugelbahnEvent >( );

	protected abstract boolean handleEvent( KugelbahnEvent e );

	protected void addElement( KugelbahnElement ke ){
		elements.add( ke );
	};

	/** Creates event in event queue
 	 *
 	 * Adds event to event queue, ordered by time. */
	public void createEvent( KugelbahnEvent e ) {
		int i;
		for( i = 0; i<eventqueue.size( ); i++ )
			if( eventqueue.get( i ).timestamp>e.timestamp )
				break;
		eventqueue.add( i,e );
	}

	private void genericHandler( KugelbahnEvent e ) {
		if( e instanceof MotorControlEvent ) {
			MotorControlEvent eprime = (MotorControlEvent)e;
			try {
				eprime.motor.setVelocity( eprime.speed );
			} catch( Exception err ) {
				System.out.println( "Error trying to set Motor Speed" );
			}
		} else if( e instanceof KugelTransferEvent ) {
			KugelTransferEvent eprime = (KugelTransferEvent)e;
			eprime.source.transferOne( );
		} else if( e instanceof KugelCreateEvent ) {
			KugelCreateEvent eprime = (KugelCreateEvent)e;
			eprime.element.kugeln.add( eprime.kugel );
		} else if( e instanceof KugelChangeEvent ) {
			KugelChangeEvent eprime = (KugelChangeEvent)e;
			int i;
			search:
			for( i = 0; i<elements.size( ); i++ ) {
				KugelbahnElement el = elements.get( i );
				int j;
				for( j = 0; j<el.kugeln.size( ); j++ )
					if( el.kugeln.get( j )==eprime.oldKugel ) {
						el.kugeln.set( j,eprime.newKugel );
						break search;
					}
			}
		} else if( e instanceof BranchChangeEvent ) {
			 BranchChangeEvent eprime = (BranchChangeEvent)e;
			 eprime.target.direction = eprime.direction;
		}
	}

	/** Process all events
 	 *
 	 * Handles all events in the stack by the handler */
	public void process( ) {
		long now = System.currentTimeMillis( );
		KugelbahnEvent e;
		while( eventqueue.size( )>0 &&( e = eventqueue.get( 0 ) ).timestamp<=now ) {
			eventqueue.remove( 0 );
			if( handleEvent( e ) )
				genericHandler( e );
		}
	}
}
