
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import kugelbahn.Kugelbahn;
import kugelbahn.KugelbahnElektronik;
import kugelbahn.KugelbahnEvent;
import kugelbahn.elektronik.IO16;
import kugelbahn.elektronik.Motor;
import kugelbahn.elektronik.Sensor;
import kugelbahn.elektronik.TFStack;
import kugelbahn.events.IntegerEvent;
import kugelbahn.events.MotorControlEvent;
import kugelbahn.events.ProgramStartEvent;
import kugelbahn.events.ProgramStopEvent;
import kugelbahn.events.SensorEvent;


public class Weichen extends Kugelbahn {
	KugelbahnElektronik elektro;

	// zur Bestimmung, wie der Status der Sensoren ist (1 oder 0)
	boolean statusSM0V = false;
	boolean statusSM1V = false;
	boolean statusSM2V = false;
	boolean statusSM3V = false;
	boolean statusSM4V = false;
	boolean statusSM5V = false;
	boolean statusSM6V = false;
	boolean statusSM7V = false;
	boolean statusSM0Z = false;
	boolean statusSM1Z = false;
	boolean statusSM2Z = false;
	boolean statusSM3Z = false;
	boolean statusSM4Z = false;
	boolean statusSM5Z = false;
	boolean statusSM6Z = false;
	boolean statusSM7Z = false;

	// zur Istbestimmung der Kugelanzahl zu Beginn der Simulation
	boolean startM1 = false;
	boolean startM2 = false;
	boolean startM3 = false;
	boolean startM4 = false;
	boolean startM5 = false;
	boolean startM6 = false;
	boolean startM7 = false;
	boolean systemIstBereit = true;

	// Positionen im Bezug zur Kugelrollrichtung
	static final int LINKS = 0;
	static final int MITTE = 1;
	static final int RECHTS = 2;
	static final int AUF = 3;
	static final int ZU = 4;
	int istPos_E1 = LINKS;
	int sollPos_E1 = RECHTS;
	int istPos_E2 = LINKS;
	int sollPos_E2 = RECHTS;
	int istPos_W0 = RECHTS;
	int sollPos_W0 = LINKS;
	int istPos_W1 = LINKS;
	int sollPos_W1 = RECHTS;
	int istPos_W2 = LINKS;
	int sollPos_W2 = RECHTS;
	int istPos_A1 = ZU;
	int sollPos_A1 = AUF;


	private Motor motor(String motorname) {
		return elektro.getMotor(motorname);
	}

	public Weichen(KugelbahnElektronik elektro) throws Exception {
		this.elektro = elektro;

		// Motoren Bezeichnun, aufruf durch "start = new
		// MotorControlEvent(-32766, motor("M1"))" 32766 ist die max.
		// Geschwindigkeit
		ArrayList<Motor> motoren = new ArrayList<Motor>();
		motoren.add(new Motor("6qBSZF", "M0"));
		motoren.add(new Motor("68aTsH", "M1"));
		motoren.add(new Motor("6e69u2", "M2"));
		motoren.add(new Motor("68xHQD", "M3"));
		motoren.add(new Motor("6e8bAe", "M4"));
		motoren.add(new Motor("6e7vxX", "M5"));
		motoren.add(new Motor("6k55vx", "M6"));
		motoren.add(new Motor("6wVDcX", "M7"));
		motoren.add(new Motor("6kpC6p", "W0"));
		motoren.add(new Motor("68cyWM", "W2"));
		motoren.add(new Motor("6etmGH", "W1"));
		motoren.add(new Motor("68cdaP", "A1"));
		motoren.add(new Motor("6JMecp", "E1"));
		motoren.add(new Motor("68cQQY", "E2"));

		ArrayList<Sensor> sensorsT = new ArrayList<>();

		sensorsT.add(new ProSenseSensor(0, "SM0V", 'a'));
		sensorsT.add(new ProSenseSensor(1, "SM0H", 'a'));///
		sensorsT.add(new ProSenseSensor(4, "SM2H", 'a'));
		sensorsT.add(new ProSenseSensor(2, "SM2V", 'a'));
		sensorsT.add(new ProSenseSensor(7, "SM4V", 'a'));
		sensorsT.add(new ProSenseSensor(3, "SM7Z", 'a'));
		sensorsT.add(new ProSenseSensor(5, "RFIDb", 'a'));
		sensorsT.add(new ProSenseSensor(6, "SW2V", 'a'));
		sensorsT.add(new ProSenseSensor(3, "SM7V", 'b'));
		sensorsT.add(new ProSenseSensor(5, "SM5Z", 'b'));
		sensorsT.add(new ProSenseSensor(0, "SM5V", 'b'));
		sensorsT.add(new ProSenseSensor(2, "SM5H", 'b'));//
		sensorsT.add(new ProSenseSensor(4, "SM3V", 'b'));
		sensorsT.add(new ProSenseSensor(7, "SM4H", 'b'));
		sensorsT.add(new ProSenseSensor(1, "RFIDg", 'b'));
		sensorsT.add(new ProSenseSensor(6, "Eil-M5-Bahn", 'b'));

		ArrayList<Sensor> sensorsD = new ArrayList<>();

		sensorsD.add(new ProSenseSensor(0, "SM1Z", 'a'));
		sensorsD.add(new ProSenseSensor(1, "SM1V", 'a'));///
		sensorsD.add(new ProSenseSensor(2, "SM3Z", 'a'));
		sensorsD.add(new ProSenseSensor(3, "SM4Z", 'a'));
		sensorsD.add(new ProSenseSensor(4, "SM6V", 'a'));//
		sensorsD.add(new ProSenseSensor(5, "SM6Z", 'a'));
		sensorsD.add(new ProSenseSensor(6, "SM2Z", 'a'));
		sensorsD.add(new ProSenseSensor(7, "SM7H", 'a'));
		sensorsD.add(new ProSenseSensor(5, "SM6H", 'b'));
		sensorsD.add(new ProSenseSensor(6, "SM1H", 'b'));
		sensorsD.add(new ProSenseSensor(7, "SM3H", 'b'));

		ArrayList<IO16> ios = new ArrayList<IO16>();

		IO16 brickletT = new IO16("qK3", sensorsT);
		IO16 brickletD = new IO16("qKa", sensorsD);

		ios.add(brickletT);
		ios.add(brickletD);

		ArrayList<String> masters = new ArrayList<String>();

		masters.add("68cxuV");
		masters.add("6xgN6P");
		masters.add("5VF9oN");

		TFStack stack = new TFStack("localhost", 4223, masters, motoren, ios);

		this.elektro.addStacks(stack);
		
		istPos_W1();

	}

	// ------------------------------------------------------------------------------------------------------------------------
	void startAllMotors(KugelbahnEvent e) {
		MotorControlEvent start;
		if (e instanceof ProgramStartEvent) {

			start = new MotorControlEvent(-26000, motor("M0"));
			createEvent(start);
		}
	}

	void stopAllMotors(KugelbahnEvent e) {
		MotorControlEvent stop;
		if (e instanceof ProgramStopEvent) {
			stop = new MotorControlEvent(0, motor("M0"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M1"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M2"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M3"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M4"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M5"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M6"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M7"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("W0"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("W1"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("W2"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("A1"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("E1"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("E2"));
			createEvent(stop);

		}

	}

	void transfering(KugelbahnEvent e){

		if (e instanceof IntegerEvent) {
			int delay = 1000;
			IntegerEvent ie = (IntegerEvent) e;
			switch (ie.value) {
			case 0:
				if (statusSM0V) {
					sollPos_A1 = ZU;
					IntegerEvent ie1 = new IntegerEvent(0, delay);
					createEvent(ie1);
					istPos_A1();
				}
				break;

			default:
				break;
			}
		}
		if (e instanceof SensorEvent) {
			SensorEvent se = (SensorEvent) e;

			// --------------------------------------------------Anzahl der
			// Aufträge vor und in der jeweiligen Maschine
			// bestimmen----------------------------------------
			// ----------M1----------------------------
			if (se.sensor.sensorname.equals("SM0V")) {
				IntegerEvent ie1 = new IntegerEvent(0, 500);
				createEvent(ie1);
				statusSM0V = se.state;

			}

			if (se.sensor.sensorname.equals("SM1V")) {
				statusSM1V = se.state;
				if (se.state) {
					sollPos_W1 = RECHTS;
					istPos_W1();
					try {
						TimeUnit.SECONDS.sleep(2);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}

				}
			}
		}
	}

	/*
	 * void intEventAus(KugelbahnEvent e){
	 * 
	 * if (e instanceof IntegerEvent){ IntegerEvent ie= (IntegerEvent)e; switch
	 * (ie.value) { case 2: anzKugelM2=6; break; case 3: anzKugelM2=0; break;
	 * case 4: anzKugelM3=4; break; case 5: anzKugelM3=0; break;
	 * 
	 * default: break; } } }
	 */

	// -----------------------------------------------------Prozess----------------------------------------------------------------------------------
	// --------------------------------------System muss betriebsbereit
	// sein-----------------------------------------------------
	void systemStatus(KugelbahnEvent e) {
		if (e instanceof SensorEvent) {
			if (!systemIstBereit && !startM1 && !startM2 && !startM3 && !startM4 && !startM5 && !startM6 && !startM7) {
				systemIstBereit = true;
				System.out.println("Der Demonstrator ist betriebsbereit");
			}
		}
	}

	// -----------------------------------------------------M0----------------------------------------------------------------------------------
	/**
	 * Methode, die alle Motoren steuern sollte
	 * 
	 * @param e
	 *            Events (Ereignisse), die zur Steuerung notwendig sind
	 * @param motorname
	 *            Der Name der Machine, die gesteuert werden soll
	 * @param maxKugelAnzVor
	 *            die maximal erlaubte Anzahl an Kugeln vor einer Maschine
	 * @param minKugelAnzVor
	 *            die minimal erlaubte Anzahl an Kugeln vor einer Maschine
	 * @param maxKugelAnzNach
	 *            die maximal erlaubte Gesamtanzahl an Kugeln vor der nächsten
	 *            Maschine
	 */
	void logicMotoren_0(KugelbahnEvent e) {
		// int maxKugelAnzVor = 6;
		// int minKugelAnzVor = 1;
		// int maxKugelAnzNach = 3;

		MotorControlEvent start;

		if (e instanceof ProgramStartEvent) {
			start = new MotorControlEvent(-26000, motor("M0"));
			createEvent(start);
		}
	}

	// ----------------------------------------------------------Weichen-initialisierung------------------------------------------------------------------------------------

	void setWeichen(KugelbahnEvent e) {
		if (e instanceof ProgramStartEvent) {

			istPos_E1();
			// System.out.println("Init E1 ist rechts");

			istPos_E2();
			// System.out.println("Init E1 ist rechts");

			istPos_W0();
			// System.out.println("Init W0 ist links");

			istPos_W1();
			// System.out.println("Init W1 ist rechts");

			istPos_W2();
			// System.out.println("Init W2 ist rechts");

			istPos_A1();
			// System.out.println("Init A1 ist zu");
		}

	}

	// -----------------------------------------------------------Weiche
	// 0-------------------------------------------------------------------------------------------
	/**
	 * Stellt die Weiche W0, nach der SollPosition um
	 *
	 * TODO:WeichenParameter richtig bestimmen
	 *
	 */
	void istPos_W0() {
		MotorControlEvent start, stop;

		if (sollPos_W0 != istPos_W0) {
			if (istPos_W0 == LINKS) {
				start = new MotorControlEvent(-32000, motor("W0"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("W0"), 500);
				createEvent(stop);
				istPos_W0 = sollPos_W0;
				System.out.println("W0 ist Rechts");
			} else {
				start = new MotorControlEvent(32000, motor("W0"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("W0"), 500);
				createEvent(stop);
				istPos_W0 = sollPos_W0;
				System.out.println("W0 ist Links");
			}
		}
	}

	// -------------------------------------------------Schranke--------------------------------------------------------------------------------

	void istPos_A1() {
		MotorControlEvent start, stop;
		if (sollPos_A1 != istPos_A1) {
			if (istPos_A1 == ZU) {
				start = new MotorControlEvent(20000, motor("A1"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("A1"), 1000);
				createEvent(stop);
				istPos_A1 = sollPos_A1;
				// System.out.println("A1 ist auf");
			} else {
				if (istPos_A1 == AUF) {
					start = new MotorControlEvent(-20000, motor("A1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("A1"), 1000);
					createEvent(stop);
					istPos_A1 = sollPos_A1;
					IntegerEvent ie = new IntegerEvent(10, 15000);
					createEvent(ie);
					// System.out.println("A1 ist zu");
				}
			}
		}

	}

	void istPos_E1() {
		MotorControlEvent start, stop;
		if (sollPos_E1 != istPos_E1) {
			if (istPos_E1 == LINKS) {
				start = new MotorControlEvent(20000, motor("E1"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("E1"), 1000);
				createEvent(stop);
				istPos_E1 = sollPos_E1;
				// System.out.println("E1 ist Rechts");
			} else if (istPos_E1 == RECHTS) {
				start = new MotorControlEvent(-20000, motor("E1"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("E1"), 1000);
				createEvent(stop);
				istPos_E1 = sollPos_E1;
				// System.out.println("E1 ist Links");
			}
		}
	}

	void istPos_E2() {
		MotorControlEvent start, stop;
		if (sollPos_E2 != istPos_E2) {
			if (istPos_E2 == LINKS) {
				start = new MotorControlEvent(-20000, motor("E2"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("E2"), 1000);
				createEvent(stop);
				istPos_E2 = sollPos_E2;
				System.out.println("E2 ist Rechts");
			} else {
				if (istPos_E2 == RECHTS) {
					start = new MotorControlEvent(20000, motor("E2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("E2"), 1000);
					createEvent(stop);
					istPos_E2 = sollPos_E2;
					System.out.println("E2 ist Links");
				}
			}
		}
	}

	// -------------------------------------------------WeicheW1--------------------------------------------------------------------------------

	/**
	 * Stellt die Weiche W1, nach der SollPosition um
	 *
	 * TODO:WeichenParameter richtig bestimmen
	 *
	 */
	void istPos_W1() {
		MotorControlEvent start, stop;
		if (sollPos_W1 != istPos_W1) {
			if (istPos_W1 == LINKS) {
				if (sollPos_W1 == RECHTS) {
					start = new MotorControlEvent(4000, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Rechts (1)");
				} else {
					start = new MotorControlEvent(2000, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Mitte(2)");

				}
			} else if (istPos_W1 == RECHTS) {
				if (sollPos_W1 == LINKS) {
					start = new MotorControlEvent(-5000, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Links(3)");
				} else {
					start = new MotorControlEvent(-2200, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Mitte(4)");

				}
			} else if (istPos_W1 == MITTE) {
				if (sollPos_W1 == LINKS) {
					start = new MotorControlEvent(-2000, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Links(5)");
				} else {
					start = new MotorControlEvent(2000, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Rechts(6)");

				}
			}

		}

	}

	// -------------------------------------------------WeicheW2--------------------------------------------------------------------------------

	/**
	 * Stellt die Weiche W2, nach der SollPosition um
	 *
	 * TODO:WeichenParameter richtig bestimmen
	 *
	 */
	void istPos_W2() {
		MotorControlEvent start, stop;
		if (sollPos_W2 != istPos_W2) {
			if (istPos_W2 == LINKS) {
				if (sollPos_W2 == RECHTS) {
					start = new MotorControlEvent(5000, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Rechts (1)");
				} else {
					start = new MotorControlEvent(1200, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Mitte(2)");

				}
			} else if (istPos_W2 == RECHTS) {
				if (sollPos_W2 == LINKS) {
					start = new MotorControlEvent(-5000, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Links(3)");
				} else {
					start = new MotorControlEvent(-1500, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Mitte(4)");

				}
			} else if (istPos_W2 == MITTE) {
				if (sollPos_W2 == LINKS) {
					start = new MotorControlEvent(-1200, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Links(5)");
				} else {
					start = new MotorControlEvent(1000, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Rechts(6)");

				}
			}

		}

	}

	/** Event Handler */
	protected boolean handleEvent(KugelbahnEvent e) {
		if (e instanceof SensorEvent) {
			SensorEvent eprime = (SensorEvent) e;
			ProSenseSensor sensorprime = (ProSenseSensor) (eprime.sensor);
			sensorprime.simulatedstate = eprime.state;
		}
		startAllMotors(e);
		setWeichen(e);
		transfering(e);
		logicMotoren_0(e);
		systemStatus(e);
		// if( e instanceof ProgramStopEvent )
		// System.out.println( "STOP!!!" );
		stopAllMotors(e);
		return true;
	}
}
