package kugelbahn;

public abstract class KugelbahnEvent {
	public long timestamp;

	public KugelbahnEvent( long time ) {
		this.timestamp = time;
	}

	public KugelbahnEvent( ) {
		this.timestamp = System.currentTimeMillis( );
	}
}

