package kugelbahn.events;

import kugelbahn.KugelbahnEvent;

public class IntegerEvent extends KugelbahnEvent {
	public int value;
	
	public IntegerEvent( int val,long delay ) {
		super( System.currentTimeMillis( )+delay );
		this.value = val;	
	}
}
