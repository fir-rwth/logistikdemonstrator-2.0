public class Auftrag {

	private int id;;
	private int losgroesse;
	private int rueckmeldeNr;
	private int maschine;

	public Auftrag(int id, int losgroeße, int rueckmeldeNr, int maschine) {
		super();
		this.id = id;
		this.losgroesse = losgroeße;
		this.rueckmeldeNr = rueckmeldeNr;
		this.maschine = maschine;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLosgroesse() {
		return losgroesse;
	}

	public void setLosgroesse(int losgroesse) {
		this.losgroesse = losgroesse;
	}

	public int getRueckmeldeNr() {
		return rueckmeldeNr;
	}

	public void setRueckmeldeNr(int rueckmeldeNr) {
		this.rueckmeldeNr = rueckmeldeNr;
	}

	public int getMaschine() {
		return maschine;
	}

	public void setMaschine(int maschine) {
		this.maschine = maschine;
	}
}