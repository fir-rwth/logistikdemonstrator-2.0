import kugelbahn.KugelbahnElement;

public class FeedbackEvent {
	public Order order;
	public KugelbahnElement section;
	public boolean type;
	
	public FeedbackEvent( Order o,KugelbahnElement s,boolean t ) {
		order = o;
		section = s;
		type = t;
	}
}
