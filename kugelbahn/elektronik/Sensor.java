package kugelbahn.elektronik;

public class Sensor {
	public int bitNumber; ///< 0-7
	public char port;
	int deadTime = 1500;
	IO16 bricklet;
	long lastOff = 0;
	public String sensorname;

	public Sensor( int bitNumber, String sensorname, char port ) {
		this.bitNumber = bitNumber;
		this.sensorname = sensorname;
		this.port = port;
	}
	
	public boolean isActive( ) {
		return( System.currentTimeMillis()-this.lastOff )>this.deadTime;
	}
	
	public void deactivate( ) {
		this.lastOff = System.currentTimeMillis();
	}
}
