package kugelbahn;

import java.util.ArrayList;
import java.util.LinkedList;

public abstract class KugelbahnElement {
	public ArrayList<KugelbahnElement> outputs = new ArrayList<KugelbahnElement>();
	public ArrayList<KugelbahnElement> inputs = new ArrayList<KugelbahnElement>();
	public LinkedList< KugelbahnKugel > kugeln = new LinkedList<>();
	
	public String ident;
	
	public void connectIn(KugelbahnElement ke) {
		inputs.add(ke);
		ke.outputs.add(this);
	}
	
	public KugelbahnKugel get( int index ) {
		return this.kugeln.get( index );
	}
	
	public KugelbahnElement(String dbgi,KugelbahnElement ... ke){
		this.ident = dbgi;
		
		for (int i = 0; i < ke.length; i++) {
			connectIn(ke[i]);
		}		
	}
	
	public void clear( ) {
		this.kugeln.clear( );
	}

	abstract public KugelbahnElement getNext();
	
	public void addKugel( KugelbahnKugel o ) {
		kugeln.add( o );
	}
	
	public KugelbahnKugel remKugel( ) {
		if( this.count()>0 ) {
			KugelbahnKugel kugel = this.kugeln.pop( );
			return kugel;
		}
		
		return null;		
	}
	
	public void transferOne( ) {
		if( this.count()>0 ) {
			this.getNext( ).addKugel( this.remKugel( ) );
		} else {
		}
	}
	
	public int count( ) {
		return this.kugeln.size( );
	}
	
	public String toString( ) {
		String result = this.ident+" [";
		for( int i = 0; i<kugeln.size( ); i++ ) {
			result += kugeln.get( i );
			if( i<kugeln.size( )-1 )
				result += ",";
		}
		result += "]";
				
		return result;
		
	}
}
