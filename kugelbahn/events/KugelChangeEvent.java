package kugelbahn.events;

import kugelbahn.KugelbahnEvent;
import kugelbahn.KugelbahnKugel;

public class KugelChangeEvent extends KugelbahnEvent {
	public KugelbahnKugel oldKugel,newKugel;
	
	public KugelChangeEvent( KugelbahnKugel oldKugel,KugelbahnKugel newKugel ) {
		this.oldKugel = oldKugel;
		this.newKugel = newKugel;
	}
}
