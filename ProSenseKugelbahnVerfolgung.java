import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import kugelbahn.Kugelbahn;
import kugelbahn.KugelbahnElektronik;
import kugelbahn.KugelbahnEvent;
import kugelbahn.elektronik.IO16;
import kugelbahn.elektronik.Motor;
import kugelbahn.elektronik.Sensor;
import kugelbahn.elektronik.TFStack;
import kugelbahn.events.IntegerEvent;
import kugelbahn.events.MotorControlEvent;
import kugelbahn.events.ProgramStartEvent;
import kugelbahn.events.ProgramStopEvent;
import kugelbahn.events.SensorEvent;
import kugelbahn.PentaDB;

public class ProSenseKugelbahnVerfolgung extends Kugelbahn {
	KugelbahnElektronik elektro;

	// zur Bestimmung, wie der Status der Sensoren ist (1 oder 0)
	boolean statusSM0V = false;
	boolean statusSM1V = false;
	boolean statusSM2V = false;
	boolean statusSM3V = false;
	boolean statusSM4V = false;
	boolean statusSM5V = false;
	boolean statusSM6V = false;
	boolean statusSM7V = false;
	boolean statusSM0Z = false;
	boolean statusSM1Z = false;
	boolean statusSM2Z = false;
	boolean statusSM3Z = false;
	boolean statusSM4Z = false;
	boolean statusSM5Z = false;
	boolean statusSM6Z = false;
	boolean statusSM7Z = false;
	boolean statusSM0H = false;

	// zur Istbestimmung der Kugelanzahl zu Beginn der Simulation
	boolean startM1 = false;
	boolean startM2 = false;
	boolean startM3 = false;
	boolean startM4 = false;
	boolean startM5 = false;
	boolean startM6 = false;
	boolean startM7 = false;
	boolean systemIstBereit = true;
	
	boolean m2finished = false;
	boolean m3started = false;

	// Positionen im Bezug zur Kugelrollrichtung
	static final int LINKS = 0;
	static final int MITTE = 1;
	static final int RECHTS = 2;
	static final int AUF = 3;
	static final int ZU = 4;
	int istPos_E1 = LINKS;
	int sollPos_E1 = RECHTS;
	int istPos_E2 = LINKS;
	int sollPos_E2 = RECHTS;
	int istPos_W0 = RECHTS;
	int sollPos_W0 = LINKS;
	int istPos_W1 = LINKS;
	int sollPos_W1 = RECHTS;
	int istPos_W2 = LINKS;
	int sollPos_W2 = RECHTS;
	int istPos_A1 = ZU;
	int sollPos_A1 = AUF;
	int W2warten = 0;
	int startw1 = 0;

	// Pfad 1 = M2,M4,M6
	// Pfad 2 = M3,M5.M6
	// Pfad 3 = M2,M7
	// Auftrag(id, losgroesse, pfad)

	// Auftrag auftrag1 = new Auftrag(1, 1, 3);
	// Auftrag auftrag2 = new Auftrag(2, 1, 2);
	// Auftrag auftrag3 = new Auftrag(3, 2, 1);
	// Auftrag auftrag4 = new Auftrag(4, 3, 2);
	// Auftrag auftrag5 = new Auftrag(5, 1, 1);
	// Auftrag auftrag6 = new Auftrag(6, 2, 3);
	// Auftrag auftrag7 = new Auftrag(7, 3, 2);
	// Auftrag auftrag8 = new Auftrag(8, 1, 1);
	// Auftrag auftrag9 = new Auftrag(9, 2, 3);
	// Auftrag auftrag10 = new Auftrag(10, 3, 2);
	//
	// Auftrag[] auftraege = { auftrag1, auftrag2, auftrag3, auftrag4, auftrag5,
	// auftrag6, auftrag7, auftrag8, auftrag9,
	// auftrag10 };

	ArrayList<Auftrag> m0 = new ArrayList<Auftrag>();
	ArrayList<Auftrag> m1 = new ArrayList<Auftrag>();
	ArrayList<Auftrag> m2 = new ArrayList<Auftrag>();
	ArrayList<Auftrag> m3 = new ArrayList<Auftrag>();
	ArrayList<Auftrag> m4 = new ArrayList<Auftrag>();
	ArrayList<Auftrag> m5 = new ArrayList<Auftrag>();
	ArrayList<Auftrag> m6 = new ArrayList<Auftrag>();
	ArrayList<Auftrag> m7 = new ArrayList<Auftrag>();

	ArrayList<Integer> w0 = new ArrayList<Integer>();
	ArrayList<Integer> w1 = new ArrayList<Integer>();
	ArrayList<Integer> w2 = new ArrayList<Integer>();
	ArrayList<Integer> w2_m3 = new ArrayList<Integer>();

	ArrayList<Auftrag> auftraege = new ArrayList<>();

	int szenario = 0;

	int pointerM0 = 0;
	int pointerM1 = 0;
	int pointerM2 = 0;
	int pointerM3 = 0;
	int pointerM4 = 0;
	int pointerM5 = 0;
	int pointerM6 = 0;
	int pointerM7 = 0;

	int anzKugelM1 = 1;
	int anzKugelM2 = 1;
	int anzKugelM3 = 1;
	int anzKugelM4 = 1;
	int anzKugelM5 = 1;
	int anzKugelM6 = 1;
	int anzKugelM7 = 1;

	int pointerW0 = 0;
	int pointerW1 = 0;
	int pointerW2 = 0;
	int pointerW2_m3 = 0;

	int laufzeitnr = 0;

	long startzeit = 0;

	boolean m6beendet = false;
	boolean m7beendet = false;

	PentaDB penta = new PentaDB();
	Connection connection = penta.connectToDB();

	private Motor motor(String motorname) {
		return elektro.getMotor(motorname);
	}

	public ProSenseKugelbahnVerfolgung(KugelbahnElektronik elektro, int szenario) throws Exception {
		this.elektro = elektro;
		this.szenario = szenario;
		// Motoren Bezeichnun, aufruf durch "start = new
		// MotorControlEvent(-32766, motor("M1"))" 32766 ist die max.
		// Geschwindigkeit
		ArrayList<Motor> motoren = new ArrayList<Motor>();
		motoren.add(new Motor("6qBSZF", "M0"));
		motoren.add(new Motor("68aTsH", "M1"));
		motoren.add(new Motor("6e69u2", "M2"));
		motoren.add(new Motor("68xHQD", "M3"));
		motoren.add(new Motor("6e8bAe", "M4"));
		motoren.add(new Motor("6e7vxX", "M5"));
		motoren.add(new Motor("6k55vx", "M6"));
		motoren.add(new Motor("6wVDcX", "M7"));
		motoren.add(new Motor("6kpC6p", "W0"));
		motoren.add(new Motor("68cyWM", "W2"));
		motoren.add(new Motor("6etmGH", "W1"));
		motoren.add(new Motor("68cdaP", "A1"));
		motoren.add(new Motor("6JMecp", "E1"));
		motoren.add(new Motor("68cQQY", "E2"));

		ArrayList<Sensor> sensorsT = new ArrayList<>();

		sensorsT.add(new ProSenseSensor(0, "SM0V", 'a'));
		sensorsT.add(new ProSenseSensor(1, "SM0H", 'a'));///
		sensorsT.add(new ProSenseSensor(4, "SM2H", 'a'));
		sensorsT.add(new ProSenseSensor(2, "SM2V", 'a'));
		sensorsT.add(new ProSenseSensor(7, "SM4V", 'a'));
		sensorsT.add(new ProSenseSensor(3, "SM7Z", 'a'));
		sensorsT.add(new ProSenseSensor(5, "RFIDb", 'a'));
		sensorsT.add(new ProSenseSensor(6, "SW2V", 'a'));
		sensorsT.add(new ProSenseSensor(3, "SM7V", 'b'));
		sensorsT.add(new ProSenseSensor(5, "SM5Z", 'b'));
		sensorsT.add(new ProSenseSensor(0, "SM5V", 'b'));
		sensorsT.add(new ProSenseSensor(2, "SM5H", 'b'));//
		sensorsT.add(new ProSenseSensor(4, "SM3V", 'b'));
		sensorsT.add(new ProSenseSensor(7, "SM4H", 'b'));
		sensorsT.add(new ProSenseSensor(1, "RFIDg", 'b'));
		sensorsT.add(new ProSenseSensor(6, "Eil-M5-Bahn", 'b'));

		ArrayList<Sensor> sensorsD = new ArrayList<>();

		sensorsD.add(new ProSenseSensor(0, "SM1Z", 'a'));
		sensorsD.add(new ProSenseSensor(1, "SM1V", 'a'));///
		sensorsD.add(new ProSenseSensor(2, "SM3Z", 'a'));
		sensorsD.add(new ProSenseSensor(3, "SM4Z", 'a'));
		sensorsD.add(new ProSenseSensor(4, "SM6V", 'a'));//
		sensorsD.add(new ProSenseSensor(5, "SM6Z", 'a'));
		sensorsD.add(new ProSenseSensor(6, "SM2Z", 'a'));
		sensorsD.add(new ProSenseSensor(7, "SM7H", 'a'));
		sensorsD.add(new ProSenseSensor(5, "SM6H", 'b'));
		sensorsD.add(new ProSenseSensor(6, "SM1H", 'b'));
		sensorsD.add(new ProSenseSensor(7, "SM3H", 'b'));

		ArrayList<IO16> ios = new ArrayList<IO16>();

		IO16 brickletT = new IO16("qK3", sensorsT);
		IO16 brickletD = new IO16("qKa", sensorsD);

		ios.add(brickletT);
		ios.add(brickletD);

		ArrayList<String> masters = new ArrayList<String>();

		masters.add("68cxuV");
		masters.add("6xgN6P");
		masters.add("5VF9oN");

		TFStack stack = new TFStack("localhost", 4223, masters, motoren, ios);

		this.elektro.addStacks(stack);

		// Pfad 1 = M2,M4,M6
		// Pfad 2 = M3,M5.M6
		// Pfad 3 = M2,M7

		Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		String query = "";
		if (szenario == 1) {
			query = "SELECT RUECKMELDE_NR, AUFTRAGSMENGE, RUECKMELDE_POSITIONS_NR, BELEGUNGSEINHEIT_NR, REIHENFOLGE_UR FROM RWTH_DEMON_PULL ORDER BY REIHENFOLGE_UR, BELEGUNGSEINHEIT_NR";
		} else {
			query = "SELECT RUECKMELDE_NR, AUFTRAGSMENGE, RUECKMELDE_POSITIONS_NR, BELEGUNGSEINHEIT_NR, REIHENFOLGE FROM RWTH_DEMON_PULL ORDER BY REIHENFOLGE, BELEGUNGSEINHEIT_NR";
		}

		System.out.println("\nExecuting query: " + query);
		ResultSet rset = stmt.executeQuery(query);
		int id = 0;
		int losgroesse = 0;
		int rueckmeldeNr = 0;
		int maschine = 0;

		while (rset.next()) {
			if (rset.getInt(3) == 10) {
				id = rset.getInt(1);
				losgroesse = rset.getInt(2);
				System.out.println(id);
				auftraege.add(new Auftrag(id, losgroesse, rset.getInt(3), rset.getInt(4)));
			}
			rueckmeldeNr = rset.getInt(3);
			maschine = rset.getInt(4);
			Auftrag auftrag = new Auftrag(id, losgroesse, rueckmeldeNr, maschine);
			switch (maschine) {
			case 10:
				m0.add(auftrag);
				break;
			case 11:
				m1.add(auftrag);
				break;
			case 12:
				m2.add(auftrag);
				w0.add(0);
				break;
			case 13:
				m3.add(auftrag);
				w0.add(2);
				break;
			case 14:
				m4.add(auftrag);
				w1.add(2);
				w2.add(2);
				break;
			case 15:
				m5.add(auftrag);
				w2_m3.add(1);
				break;
			case 16:
				m6.add(auftrag);
				break;
			case 17:
				m7.add(auftrag);
				w1.add(1);
				break;
			default:
				System.out.println("Ung�ltige Bezeichnung in Spalte BELEGUNGSEINHEIT");
				break;
			}

		}
		for (int i=0; i< m1.size();i++){
		System.out.println("M1: " + m1.get(i).getId());
	}
		// System.out.println(rset.getString(2));
		// int pfad = 0;
		// int losgroesse = 0;
		// if (rset.getString(2).equals("Kart_eGo_neu")) {
		// pfad = 1;
		// losgroesse = 1;
		// System.out.println("Kart");
		// } else if (rset.getString(2).equals("Life_eGo_neu")) {
		// pfad = 2;
		// losgroesse = 2;
		// System.out.println("Life");
		// } else if (rset.getString(2).equals("Fit_eGo_neu")) {
		// pfad = 3;
		// losgroesse = 3;
		// System.out.println("Fit");
		// }
		// auftraege.add(new Auftrag(Integer.parseInt(rset.getString(1)),
		// losgroesse, pfad));
		// }
		//
		// for (int i = 0; i < auftraege.size(); i++) {
		// m1.add(auftraege.get(i));
		//
		// if (auftraege.get(i).getPfad() == 1) {
		// w0.add(0);
		// m2.add(auftraege.get(i));
		// w1.add(2);
		// w2.add(2);
		// m4.add(auftraege.get(i));
		// m6.add(auftraege.get(i));
		// }
		//
		// if (auftraege.get(i).getPfad() == 2) {
		// w0.add(2);
		// m3.add(auftraege.get(i));
		// w2.add(1);
		// m5.add(auftraege.get(i));
		// m6.add(auftraege.get(i));
		// }
		//
		// if (auftraege.get(i).getPfad() == 3) {
		// w0.add(0);
		// m2.add(auftraege.get(i));
		// w1.add(1);
		// m7.add(auftraege.get(i));
		// }
		//
		// }

		stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		query = "SELECT MAX(LFD_SZEN_NR) FROM RWTH_DEMON_PUSH";
		System.out.println("\nExecuting query: " + query);
		rset = stmt.executeQuery(query);
		if (rset.next()) {
			if (szenario == 1) {
				System.out.println("Szenario 1");
				laufzeitnr = rset.getInt(1) + 1;
			} else {
				laufzeitnr = rset.getInt(1);
			}
			System.out.println("Laufzeitnr: " + laufzeitnr);
		}

		istPos_W1();
	}

	// ------------------------------------------------------------------------------------------------------------------------
	void startAllMotors(KugelbahnEvent e) {
		MotorControlEvent start;
		if (e instanceof ProgramStartEvent) {

			start = new MotorControlEvent(-26000, motor("M0"));
			createEvent(start);
		}
	}

	void stopAllMotors(KugelbahnEvent e) {
		MotorControlEvent stop;
		if (e instanceof ProgramStopEvent) {
			stop = new MotorControlEvent(0, motor("M0"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M1"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M2"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M3"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M4"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M5"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M6"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("M7"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("W0"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("W1"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("W2"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("A1"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("E1"));
			createEvent(stop);
			stop = new MotorControlEvent(0, motor("E2"));
			createEvent(stop);

		}

	}

	void setMotorspeed(int losgroesse, String motorname) {
		int speed = 0;

		switch (losgroesse) {
		case 1:
			speed = 28000;
			break;
		case 3:
			speed = 25000;
			break;
		case 9:
			speed = 22000;
			break;
		default:
			speed = 0;
			System.out.println("Ungültiger Bezeichner in Spalte AUFTRAGSMENGE");
			break;
		}

		if (motorname == "M1" || motorname == "M2" || motorname == "M7") {
			speed = -speed;
		}

		System.out.println("Losgroesse: " + losgroesse + " Motorname: " + motorname + " Speed: " + speed);
		MotorControlEvent start = new MotorControlEvent(speed, motor(motorname));
		createEvent(start);
	}

	void insertIntoDB(ArrayList<Auftrag> motorArray, int pointer, int rueckmeldeNr, int maschine, int step) throws SQLException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Statement stmt = connection.createStatement();
		String query = "INSERT INTO RWTH_DEMON_PUSH (idnr,LFD_SZEN_NR, ZEITSTEMPEL, RUECKMELDE_NR, RUECKMELDE_KZ, RUECKMELDE_POSITIONS_NR, BELEGUNGSEINHEIT_NR, SZENARIO) VALUES (rwth_demon_zaehler.nextval, "
				+ laufzeitnr + ", " + sdf.format(System.currentTimeMillis()) + ", " + motorArray.get(pointer).getId()
				+ ", " + step + ", " + rueckmeldeNr + ", " + maschine + ", " + szenario + ")";
		//System.out.println("\nExecuting query: " + query);
		stmt.executeUpdate(query);
	}

	void transfering(KugelbahnEvent e) throws SQLException {

		if (e instanceof IntegerEvent) {
			int delay = 1000;
			IntegerEvent ie = (IntegerEvent) e;
			switch (ie.value) {
			case 0:
				if (statusSM0V) {
					sollPos_A1 = ZU;
					IntegerEvent ie1 = new IntegerEvent(0, delay);
					createEvent(ie1);
					istPos_A1();
				}
				break;
			case 1:
				if (m2finished && !m3started) {
					setMotorspeed(m3.get(pointerM3-1).getLosgroesse(), "M3");
					m3started = true;
				}else{
					IntegerEvent ie2 = new IntegerEvent(1, 5000);
					createEvent(ie2);
				}
				
				break;

			default:
				break;
			
			}
		}

		if (e instanceof SensorEvent) {
			SensorEvent se = (SensorEvent) e;

			// --------------------------------------------------Anzahl der
			// Aufträge vor und in der jeweiligen Maschine
			// bestimmen----------------------------------------
			// ----------M1----------------------------
			if (se.sensor.sensorname.equals("SM0H")) {
				statusSM0H = se.state;
				if (se.state) {
					// sollPos_W2 = LINKS;
					// istPos_W2();
					if (startzeit == 0) {
						startzeit = System.nanoTime();
						System.out.println("Zeitmessung gestartet");
					}
					System.out.println("Auftrag " + m0.get(pointerM0).getId() + " hat den Wareneingang verlassen");
					insertIntoDB(m0, pointerM0, m0.get(pointerM0).getRueckmeldeNr(), m0.get(pointerM0).getMaschine(), 2);
					insertIntoDB(m0, pointerM0, m0.get(pointerM0).getRueckmeldeNr(), m0.get(pointerM0).getMaschine(), 3);
					pointerM0++;

				}
			}
			if (se.sensor.sensorname.equals("SM1V")) {
				statusSM1V = se.state;
				if (se.state) {
					// sollPos_W2 = MITTE;
					// istPos_W2();
					// if (startw1 == 0) {
					// MotorControlEvent start = new MotorControlEvent(5000,
					// motor("W1"));
					// createEvent(start);
					// MotorControlEvent stop = new MotorControlEvent(0,
					// motor("W1"), 1000);
					// createEvent(stop);
					// start = new MotorControlEvent(5000, motor("W2"));
					// createEvent(start);
					// stop = new MotorControlEvent(0, motor("W2"), 1000);
					// createEvent(stop);
					// istPos_W1 = RECHTS;
					// start = new MotorControlEvent(30000, motor("E1"));
					// createEvent(start);
					// stop = new MotorControlEvent(0, motor("E1"), 1000);
					// createEvent(stop);
					// istPos_E1 = RECHTS;
					//
					// startw1++;
					//
					// }
					if (pointerM1 == 0) {
						setMotorspeed(m1.get(pointerM1).getLosgroesse(), "M1");
						System.out.println("Auftrag " + m1.get(pointerM1).getId() + " bei M1 angekommen");
						insertIntoDB(m1, pointerM1, m1.get(pointerM1).getRueckmeldeNr(), m1.get(pointerM1).getMaschine(), 1);
						pointerM1++;
						System.out.println(pointerM1);
					} else {
						System.out.println("Auftrag " + m1.get(anzKugelM1).getId() + " bei M1 angekommen");
						insertIntoDB(m1, anzKugelM1, m1.get(anzKugelM1).getRueckmeldeNr(), m1.get(anzKugelM1).getMaschine(), 1);
						anzKugelM1++;
						
						if (anzKugelM1 >= auftraege.size()) {
							MotorControlEvent stop = new MotorControlEvent(0, motor("M0"), 0);
							createEvent(stop);

						}
					}
				}
			}

			if (se.sensor.sensorname.equals("SM1Z") && !se.state) {
				// sollPos_W2 = RECHTS;
				// istPos_W2();
				System.out.println("Auftrag " + m1.get(pointerM1 - 1).getId() + " wird an Maschine M1 bearbeitet");
				insertIntoDB(m1, pointerM1 - 1, m1.get(pointerM1-1).getRueckmeldeNr(), m1.get(pointerM1-1).getMaschine(), 2);
			}

			// ------M2----------------------------------------------
			if (se.sensor.sensorname.equals("SM2V")) {
				statusSM2V = se.state;
				if (se.state) {
					if (pointerM2 == 0) {
						setMotorspeed(m2.get(pointerM2).getLosgroesse(), "M2");
						System.out.println("Auftrag " + m2.get(pointerM2).getId() + "  bei M2 angekommen");
						insertIntoDB(m2, pointerM2, m2.get(pointerM2).getRueckmeldeNr(), m2.get(pointerM2).getMaschine(), 1);
						pointerM2++;
					} else {
						System.out.println("Auftrag " + m2.get(anzKugelM2).getId() + " bei M2 angekommen");
						insertIntoDB(m2, anzKugelM2, m2.get(anzKugelM2).getRueckmeldeNr(), m2.get(anzKugelM2).getMaschine(), 1);
						anzKugelM2++;
					}
				}
			}
			if (se.sensor.sensorname.equals("SM2Z")) {
				if (se.state) {
					System.out.println("Auftrag " + m2.get(pointerM2 - 1).getId() + " wird an Maschine M2 bearbeitet");
					insertIntoDB(m2, pointerM2 - 1, m2.get(pointerM2-1).getRueckmeldeNr(), m2.get(pointerM2-1).getMaschine(), 2);
					sollPos_W1 = w1.get(pointerW1);
					istPos_W1();
					if (w1.get(pointerW1) == 2) {
						sollPos_W2 = w2.get(pointerW2);
						istPos_W2();
						pointerW2++;
					}
					pointerW1++;
				}
			}

			// ----------------------------------M3--------------------------------------
			if (se.sensor.sensorname.equals("SM3V")) {
				statusSM3V = se.state;
				if (se.state) {
					if (pointerM3 == 0) {
						if (m2finished == true){
							setMotorspeed(m3.get(pointerM3).getLosgroesse(), "M3");
							pointerM3++;
						}else{
							IntegerEvent ie = new IntegerEvent(1, 5000);
							createEvent(ie);
							pointerM3++;
						}
						System.out.println("Auftrag " + m3.get(pointerM3-1).getId() + " bei M3 angekommen");
						insertIntoDB(m3, pointerM3-1, m3.get(pointerM3-1).getRueckmeldeNr(), m3.get(pointerM3-1).getMaschine(), 1);
					} else {
						System.out.println("Auftrag " + m3.get(anzKugelM3).getId() + " bei M3 angekommen");
						insertIntoDB(m3, anzKugelM3, m3.get(anzKugelM3).getRueckmeldeNr(), m3.get(anzKugelM3).getMaschine(), 1);
						anzKugelM3++;
					}
				}
			}
			if (se.sensor.sensorname.equals("SM3Z")) {
				if (se.state) {
					System.out.println("Auftrag " + m3.get(pointerM3 - 1).getId() + " wird an Maschine M3 bearbeitet");
					insertIntoDB(m3, pointerM3 - 1, m3.get(pointerM3-1).getRueckmeldeNr(), m3.get(pointerM3-1).getMaschine(), 2);
				}
			}
			// --------------------------------------M4----------------------------
			if (se.sensor.sensorname.equals("SM4V")) {
				statusSM4V = se.state;
				if (se.state) {
					if (pointerM4 == 0) {
						setMotorspeed(m4.get(pointerM4).getLosgroesse(), "M4");
						System.out.println("Auftrag " + m4.get(pointerM4).getId() + " bei M4 angekommen");
						insertIntoDB(m4, pointerM4, m4.get(pointerM4).getRueckmeldeNr(), m4.get(pointerM4).getMaschine(), 1);
						pointerM4++;
					} else {
						System.out.println("Auftrag " + m4.get(anzKugelM4).getId() + " bei M4 angekommen");
						insertIntoDB(m4, anzKugelM4, m4.get(anzKugelM4).getRueckmeldeNr(), m4.get(anzKugelM4).getMaschine(), 1);
						anzKugelM4++;
					}
				}
			}
			if (se.sensor.sensorname.equals("SM4Z")) {
				if (se.state) {
					System.out.println("Auftrag " + m4.get(pointerM4 - 1).getId() + " wird an Maschine M4 bearbeitet");
					insertIntoDB(m4, pointerM4 - 1, m4.get(pointerM4-1).getRueckmeldeNr(), m4.get(pointerM4-1).getMaschine(), 2);
				}
			}
			// ----------------------------M5---------------------------------
			if (se.sensor.sensorname.equals("SM5V")) {
				statusSM5V = se.state;
				if (se.state) {
					if (pointerM5 == 0) {
						setMotorspeed(m5.get(pointerM5).getLosgroesse(), "M5");
						System.out.println("Auftrag " + m5.get(pointerM5).getId() + " bei M5 angekommen");
						insertIntoDB(m5, pointerM5, m5.get(pointerM5).getRueckmeldeNr(), m5.get(pointerM5).getMaschine(), 1);
						pointerM5++;
					} else {
						System.out.println("Auftrag " + m5.get(anzKugelM5).getId() + " bei M5 angekommen");
						insertIntoDB(m5, anzKugelM5, m5.get(anzKugelM5).getRueckmeldeNr(), m5.get(anzKugelM5).getMaschine(), 1);
						anzKugelM5++;
					}
				}
			}
			if (se.sensor.sensorname.equals("SM5Z")) {
				if (se.state) {
					System.out.println("Auftrag " + m5.get(pointerM5 - 1).getId() + " wird an Maschine M5 bearbeitet");
					insertIntoDB(m5, pointerM5 - 1, m5.get(pointerM5-1).getRueckmeldeNr(), m5.get(pointerM5-1).getMaschine(), 2);
				}
			}
			// ---------------------------M6-----------------------------------
			if (se.sensor.sensorname.equals("SM6V")) {
				statusSM6V = se.state;
				if (se.state) {
					if (pointerM6 == 0) {
						setMotorspeed(m6.get(pointerM6).getLosgroesse(), "M6");
						System.out.println("Auftrag " + m6.get(pointerM6).getId() + " bei M6 angekommen");
						insertIntoDB(m6, pointerM6, m6.get(pointerM6).getRueckmeldeNr(), m6.get(pointerM6).getMaschine(), 1);
						pointerM6++;
					} else {
						System.out.println("Auftrag " + m6.get(anzKugelM6).getId() + " bei M6 angekommen");
						insertIntoDB(m6, anzKugelM6, m6.get(anzKugelM6).getRueckmeldeNr(), m6.get(anzKugelM6).getMaschine(), 1);
						anzKugelM6++;
					}
				}
			}
			if (se.sensor.sensorname.equals("SM6Z")) {
				if (se.state) {
					System.out.println("Auftrag " + m6.get(pointerM6 - 1).getId() + " wird an Maschine M6 bearbeitet");
					insertIntoDB(m6, pointerM6 - 1, m6.get(pointerM6-1).getRueckmeldeNr(), m6.get(pointerM6-1).getMaschine(), 2);
				}
			}
			// ------------------------M7----------------------------------
			if (se.sensor.sensorname.equals("SM7V")) {
				statusSM7V = se.state;
				if (se.state) {
					if (pointerM7 == 0) {
						setMotorspeed(m7.get(pointerM7).getLosgroesse(), "M7");
						System.out.println("Auftrag " + m7.get(pointerM7).getId() + " bei M7 angekommen");
						insertIntoDB(m7, pointerM7, m7.get(pointerM7).getRueckmeldeNr(), m7.get(pointerM7).getMaschine(), 1);
						pointerM7++;
					} else {
						System.out.println("Auftrag " + m7.get(anzKugelM7).getId() + " bei M7 angekommen");
						insertIntoDB(m7, anzKugelM7, m7.get(anzKugelM7).getRueckmeldeNr(), m7.get(anzKugelM7).getMaschine(), 1);
						anzKugelM7++;
					}
				}
			}
			if (se.sensor.sensorname.equals("SM7Z")) {
				if (se.state) {
					System.out.println("Auftrag " + m7.get(pointerM7 - 1).getId() + " wird an Maschine M7 bearbeitet");
					insertIntoDB(m7, pointerM7 - 1, m7.get(pointerM7-1).getRueckmeldeNr(), m7.get(pointerM7-1).getMaschine(), 2);
				}
			}
			if (se.sensor.sensorname.equals("SM1H")) {
				if (se.state) {
					sollPos_W0 = w0.get(pointerW0);
					istPos_W0();
					pointerW0++;
					System.out
							.println("Auftrag:" + m1.get(pointerM1 - 1).getId() + " an Maschine M1 fertig bearbeitet");
					insertIntoDB(m1, pointerM1 - 1, m1.get(pointerM1-1).getRueckmeldeNr(), m1.get(pointerM1-1).getMaschine(), 3);
					if (pointerM1 < m1.size()) {
						setMotorspeed(m1.get(pointerM1).getLosgroesse(), "M1");
						pointerM1++;
						System.out.println(pointerM1);
					} else {
						MotorControlEvent stop = new MotorControlEvent(0, motor("M1"), 3000);
						createEvent(stop);
					}
				}
			}
			if (se.sensor.sensorname.equals("SM2H")) {
				if (se.state) {

					System.out
							.println("Auftrag:" + m2.get(pointerM2 - 1).getId() + " an Maschine M2 fertig bearbeitet");
					insertIntoDB(m2, pointerM2 - 1, m2.get(pointerM2-1).getRueckmeldeNr(), m2.get(pointerM2-1).getMaschine(), 3);
					if (pointerM2 < m2.size()) {
						setMotorspeed(m2.get(pointerM2).getLosgroesse(), "M2");
						pointerM2++;
					} else {
						MotorControlEvent stop = new MotorControlEvent(0, motor("M2"), 5000);
						createEvent(stop);
						m2finished = true;
					}
				}
			}

			if (se.sensor.sensorname.equals("SM3H")) {
				if (se.state) {
					sollPos_W2 = w2_m3.get(pointerW2_m3);
					istPos_W2();
					pointerW2_m3++;
					System.out
							.println("Auftrag:" + m3.get(pointerM3 - 1).getId() + " an Maschine M3 fertig bearbeitet");
					insertIntoDB(m3, pointerM3 - 1, m3.get(pointerM3-1).getRueckmeldeNr(), m3.get(pointerM3-1).getMaschine(), 3);
					if (pointerM3 < m3.size()) {
						if (m2finished == true){
							setMotorspeed(m3.get(pointerM3).getLosgroesse(), "M3");
							pointerM3++;
						}else{
							IntegerEvent ie = new IntegerEvent(1, 5000);
							createEvent(ie);
						}
						
					} else {
						MotorControlEvent stop = new MotorControlEvent(0, motor("M3"), 3000);
						createEvent(stop);
					}
				}
			}

			if (se.sensor.sensorname.equals("SM4H")) {
				if (se.state) {
					System.out
							.println("Auftrag:" + m4.get(pointerM4 - 1).getId() + " an Maschine M4 fertig bearbeitet");
					insertIntoDB(m4, pointerM4 - 1, m4.get(pointerM4-1).getRueckmeldeNr(), m4.get(pointerM4-1).getMaschine(), 3);
					if (pointerM4 < m4.size()) {
						setMotorspeed(m4.get(pointerM4).getLosgroesse(), "M4");
						pointerM4++;
					} else {
						MotorControlEvent stop = new MotorControlEvent(0, motor("M4"), 3000);
						createEvent(stop);
					}
				}
			}

			if (se.sensor.sensorname.equals("SM5H")) {
				if (se.state) {
					System.out
							.println("Auftrag:" + m5.get(pointerM5 - 1).getId() + " an Maschine M5 fertig bearbeitet");
					insertIntoDB(m5, pointerM5 - 1, m5.get(pointerM5-1).getRueckmeldeNr(), m5.get(pointerM5-1).getMaschine(), 3);
					if (pointerM5 < m5.size()) {
						setMotorspeed(m5.get(pointerM5).getLosgroesse(), "M5");
						pointerM5++;
					} else {
						MotorControlEvent stop = new MotorControlEvent(0, motor("M5"), 3000);
						createEvent(stop);
					}
				}
			}

			if (se.sensor.sensorname.equals("SM6H")) {
				if (se.state) {
					System.out
							.println("Auftrag:" + m6.get(pointerM6 - 1).getId() + " an Maschine M6 fertig bearbeitet");
					insertIntoDB(m6, pointerM6 - 1, m6.get(pointerM6-1).getRueckmeldeNr(), m6.get(pointerM6-1).getMaschine(), 3);
					if (pointerM6 < m6.size()) {
						setMotorspeed(m6.get(pointerM6).getLosgroesse(), "M6");
						pointerM6++;
					} else {
						MotorControlEvent stop = new MotorControlEvent(0, motor("M6"), 3000);
						createEvent(stop);
						m6beendet = true;
						if (m7beendet) {
							long endzeit = System.nanoTime();
							long durchlaufzeit = (endzeit - startzeit) / 1000000000;
							System.out.println(
									"Szenario " + szenario + " beendet in " + (int) durchlaufzeit + " Sekunden.");
							return;
						}
					}
				}
			}
			if (se.sensor.sensorname.equals("SM7H")) {
				if (se.state) {
					System.out
							.println("Auftrag:" + m7.get(pointerM7 - 1).getId() + " an Maschine M7 fertig bearbeitet");
					insertIntoDB(m7, pointerM7 - 1, m7.get(pointerM7-1).getRueckmeldeNr(), m7.get(pointerM7-1).getMaschine(), 3);
					if (pointerM7 < m7.size()) {
						setMotorspeed(m7.get(pointerM7).getLosgroesse(), "M7");
						pointerM7++;
					} else {
						MotorControlEvent stop = new MotorControlEvent(0, motor("M7"), 3000);
						createEvent(stop);
						m7beendet = true;
						if (m6beendet) {
							long endzeit = System.nanoTime();
							long durchlaufzeit = (endzeit - startzeit) / 1000000000;
							System.out.println(
									"Szenario " + szenario + " beendet in " + (int) durchlaufzeit + " Sekunden.");
							return;
						}
					}
				}
			}
		}

	}

	/*
	 * void intEventAus(KugelbahnEvent e){
	 * 
	 * if (e instanceof IntegerEvent){ IntegerEvent ie= (IntegerEvent)e; switch
	 * (ie.value) { case 2: anzKugelM2=6; break; case 3: anzKugelM2=0; break;
	 * case 4: anzKugelM3=4; break; case 5: anzKugelM3=0; break;
	 * 
	 * default: break; } } }
	 */

	// -----------------------------------------------------Prozess----------------------------------------------------------------------------------
	// --------------------------------------System muss betriebsbereit
	// sein-----------------------------------------------------
	void systemStatus(KugelbahnEvent e) {
		if (e instanceof SensorEvent) {
			if (!systemIstBereit && !startM1 && !startM2 && !startM3 && !startM4 && !startM5 && !startM6 && !startM7) {
				systemIstBereit = true;
				System.out.println("Der Demonstrator ist betriebsbereit");
			}
		}
	}

	// -----------------------------------------------------M0----------------------------------------------------------------------------------
	/**
	 * Methode, die alle Motoren steuern sollte
	 * 
	 * @param e
	 *            Events (Ereignisse), die zur Steuerung notwendig sind
	 * @param motorname
	 *            Der Name der Machine, die gesteuert werden soll
	 * @param maxKugelAnzVor
	 *            die maximal erlaubte Anzahl an Kugeln vor einer Maschine
	 * @param minKugelAnzVor
	 *            die minimal erlaubte Anzahl an Kugeln vor einer Maschine
	 * @param maxKugelAnzNach
	 *            die maximal erlaubte Gesamtanzahl an Kugeln vor der nächsten
	 *            Maschine
	 */
	void logicMotoren_0(KugelbahnEvent e) {
		// int maxKugelAnzVor = 6;
		// int minKugelAnzVor = 1;
		// int maxKugelAnzNach = 3;

		MotorControlEvent start;

		if (e instanceof ProgramStartEvent) {
			start = new MotorControlEvent(-26000, motor("M0"));
			createEvent(start);
		}
	}

	// ----------------------------------------------------------Weichen-initialisierung------------------------------------------------------------------------------------

	void setWeichen(KugelbahnEvent e) {
		if (e instanceof ProgramStartEvent) {

			istPos_E1();
			// System.out.println("Init E1 ist rechts");

			istPos_E2();
			// System.out.println("Init E1 ist rechts");

			istPos_W0();
			// System.out.println("Init W0 ist links");

			istPos_W1();
			// System.out.println("Init W1 ist rechts");

			istPos_W2();
			// System.out.println("Init W2 ist rechts");

			istPos_A1();
			// System.out.println("Init A1 ist zu");
		}

	}

	// -----------------------------------------------------------Weiche 0
	// -------------------------------------------------------------------------------------------
	/**
	 * Stellt die Weiche W0, nach der SollPosition um
	 *
	 * TODO:WeichenParameter richtig bestimmen
	 *
	 */
	void istPos_W0() {
		MotorControlEvent start, stop;

		if (sollPos_W0 != istPos_W0) {
			if (istPos_W0 == LINKS) {
				start = new MotorControlEvent(-32000, motor("W0"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("W0"), 500);
				createEvent(stop);
				istPos_W0 = sollPos_W0;
				System.out.println("W0 ist Rechts");
			} else {
				start = new MotorControlEvent(32000, motor("W0"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("W0"), 500);
				createEvent(stop);
				istPos_W0 = sollPos_W0;
				System.out.println("W0 ist Links");
			}
		}
	}

	// -------------------------------------------------Schranke--------------------------------------------------------------------------------

	void istPos_A1() {
		MotorControlEvent start, stop;
		if (sollPos_A1 != istPos_A1) {
			if (istPos_A1 == ZU) {
				start = new MotorControlEvent(20000, motor("A1"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("A1"), 1000);
				createEvent(stop);
				istPos_A1 = sollPos_A1;
				// System.out.println("A1 ist auf");
			} else {
				if (istPos_A1 == AUF) {
					start = new MotorControlEvent(-20000, motor("A1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("A1"), 1000);
					createEvent(stop);
					istPos_A1 = sollPos_A1;
					IntegerEvent ie = new IntegerEvent(10, 15000);
					createEvent(ie);
					// System.out.println("A1 ist zu");
				}
			}
		}

	}

	void istPos_E1() {
		MotorControlEvent start, stop;
		if (sollPos_E1 != istPos_E1) {
			if (istPos_E1 == LINKS) {
				start = new MotorControlEvent(20000, motor("E1"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("E1"), 1000);
				createEvent(stop);
				istPos_E1 = sollPos_E1;
				// System.out.println("E1 ist Rechts");
			} else if (istPos_E1 == RECHTS) {
				start = new MotorControlEvent(-20000, motor("E1"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("E1"), 1000);
				createEvent(stop);
				istPos_E1 = sollPos_E1;
				// System.out.println("E1 ist Links");
			}
		}
	}

	void istPos_E2() {
		MotorControlEvent start, stop;
		if (sollPos_E2 != istPos_E2) {
			if (istPos_E2 == LINKS) {
				start = new MotorControlEvent(-20000, motor("E2"));
				createEvent(start);
				stop = new MotorControlEvent(0, motor("E2"), 1000);
				createEvent(stop);
				istPos_E2 = sollPos_E2;
				System.out.println("E2 ist Rechts");
			} else {
				if (istPos_E2 == RECHTS) {
					start = new MotorControlEvent(20000, motor("E2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("E2"), 1000);
					createEvent(stop);
					istPos_E2 = sollPos_E2;
					System.out.println("E2 ist Links");
				}
			}
		}
	}

	// -------------------------------------------------WeicheW1--------------------------------------------------------------------------------

	/**
	 * Stellt die Weiche W1, nach der SollPosition um
	 *
	 * TODO:WeichenParameter richtig bestimmen
	 *
	 */
	void istPos_W1() {
		MotorControlEvent start, stop;
		if (sollPos_W1 != istPos_W1) {
			if (istPos_W1 == LINKS) {
				if (sollPos_W1 == RECHTS) {
					start = new MotorControlEvent(-3000, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Rechts (1)");
				} else {
					start = new MotorControlEvent(-1700, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Mitte(2)");

				}
			} else if (istPos_W1 == RECHTS) {
				if (sollPos_W1 == LINKS) {
					start = new MotorControlEvent(3000, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Links(3)");
				} else {
					start = new MotorControlEvent(1500, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Mitte(4)");

				}
			} else if (istPos_W1 == MITTE) {
				if (sollPos_W1 == LINKS) {
					start = new MotorControlEvent(2000, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Links(5)");
				} else {
					start = new MotorControlEvent(-2000, motor("W1"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W1"), 1000);
					createEvent(stop);
					istPos_W1 = sollPos_W1;
					System.out.println("W1 ist Rechts(6)");

				}
			}

		}

	}

	// -------------------------------------------------WeicheW2--------------------------------------------------------------------------------

	/**
	 * Stellt die Weiche W2, nach der SollPosition um
	 *
	 * TODO:WeichenParameter richtig bestimmen
	 *
	 */
	void istPos_W2() {
		MotorControlEvent start, stop;
		if (sollPos_W2 != istPos_W2) {
			if (istPos_W2 == LINKS) {
				if (sollPos_W2 == RECHTS) {
					start = new MotorControlEvent(2500, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Rechts (1)");
				} else {
					start = new MotorControlEvent(1400, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Mitte(2)");

				}
			} else if (istPos_W2 == RECHTS) {
				if (sollPos_W2 == LINKS) {
					start = new MotorControlEvent(-2500, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Links(3)");
				} else {
					start = new MotorControlEvent(-1400, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Mitte(4)");

				}
			} else if (istPos_W2 == MITTE) {
				if (sollPos_W2 == LINKS) {
					start = new MotorControlEvent(-2000, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Links(5)");
				} else {
					start = new MotorControlEvent(2000, motor("W2"));
					createEvent(start);
					stop = new MotorControlEvent(0, motor("W2"), 1000);
					createEvent(stop);
					istPos_W2 = sollPos_W2;
					System.out.println("W2 ist Rechts(6)");

				}
			}

		}

	}

	/** Event Handler */
	protected boolean handleEvent(KugelbahnEvent e) {
		if (e instanceof SensorEvent) {
			SensorEvent eprime = (SensorEvent) e;
			ProSenseSensor sensorprime = (ProSenseSensor) (eprime.sensor);
			sensorprime.simulatedstate = eprime.state;
		}
		startAllMotors(e);
		setWeichen(e);
		try {
			transfering(e);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		logicMotoren_0(e);
		systemStatus(e);
		// if( e instanceof ProgramStopEvent )
		// System.out.println( "STOP!!!" );
		stopAllMotors(e);
		return true;
	}
}
