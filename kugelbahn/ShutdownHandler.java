package kugelbahn;

import kugelbahn.events.ProgramStopEvent;

public class ShutdownHandler implements Runnable {
	Kugelbahn kugelbahn;

	public ShutdownHandler( Kugelbahn kugelbahn ) {
		this.kugelbahn = kugelbahn;
	}

	public void run( ) {
		this.kugelbahn.createEvent( new ProgramStopEvent( ) );
		// Process ProgramStopEvent
		this.kugelbahn.process( );

		// Process Events created by ProgramStopEvent
		this.kugelbahn.process( );
	}
}
