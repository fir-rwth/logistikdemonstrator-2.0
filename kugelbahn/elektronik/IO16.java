package kugelbahn.elektronik;

import java.util.ArrayList;

import com.tinkerforge.*;

public class IO16 {
	public String brickletname;
	public ArrayList<Sensor> sensors;
	TFStack stack;
	public BrickletIO16 tfobject;
	public int[] sensorMasks = { 0,0 }; ///< First corresponds to port 'a', second to port 'b'
	public int[] currentvalue = { 0,0 }; ///< Last measured values in thread

	public IO16(String brickletname, ArrayList<Sensor> sensors) {
		this.brickletname = brickletname;
		this.sensors = sensors;
		
		for (int j = 0; j < this.sensors.size(); j++) {
			Sensor sensor = this.sensors.get(j);
			sensorMasks[ sensor.port=='b'?1:0 ] |= 1 << sensor.bitNumber;
		}
	}
}
