package kugelbahn.elektronik;

import java.util.ArrayList;
import kugelbahn.KugelbahnElektronik;

public class TFStack {
	public String host;
	public int port;
	
	public ArrayList <String> bricknames;
	public ArrayList<Motor> motoren;
	public ArrayList<IO16> io16bricklets;
	
	KugelbahnElektronik ke;

	public TFStack(String host, int port,ArrayList<String> bricknames,ArrayList<Motor> motoren,	ArrayList<IO16> sensorenstack) {
		this.host = host;
		this.port = port;
		this.motoren=motoren;
		this.io16bricklets=sensorenstack;
		this.bricknames = bricknames;
	}
}
