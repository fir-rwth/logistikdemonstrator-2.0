package kugelbahn.elements;

import kugelbahn.KugelbahnElement;
import kugelbahn.KugelbahnKugel;

public class Union extends KugelbahnElement {
	public Union( String dbgi,KugelbahnElement ... ke ) {
		super( dbgi,ke );
	}
	
	public KugelbahnElement getNext() {
		return this.outputs.get( 0 );
	}
	
	public void addKugel( KugelbahnKugel o ) {
		this.getNext().addKugel( o );
	}
}
